using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using WPFTemplateLib.WpfConverters.Core;
/*
* 源码已托管：https://gitee.com/dlgcy/WPFTemplateLib
*/
namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [多值转换器] 是否全为真
	/// </summary>
	/// <remarks>
	///	bool 类型直接判断；string 类型判断IsNullOrWhiteSpace；其它类型使用 double.TryParse 转换，判断是否大于零。
	/// </remarks>
	public class IsAllTrueConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values == null || values.Length == 0)
				throw new ArgumentNullException(nameof(values), "values can not be null or empty");

			bool result = false;
			List<bool> boolList = ConverterHelper.ConvertValuesToBoolList(values);
			if (boolList.TrueForAll(x => x))
			{
				result = true;
			}

			return result;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// [多值转换器] 是否全为假
	/// </summary>
	/// <remarks>
	///	bool 类型直接判断；string 类型判断IsNullOrWhiteSpace；其它类型使用 double.TryParse 转换，判断是否大于零。
	/// </remarks>
	public class IsAllFalseConverter : IMultiValueConverter
	{
		/// <inheritdoc />
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if(values == null || values.Length == 0)
				throw new ArgumentNullException(nameof(values), "values can not be null or empty");

			bool result = false;
			List<bool> boolList = ConverterHelper.ConvertValuesToBoolList(values);
			if(boolList.All(x => !x))
			{
				result = true;
			}

			return result;
		}

		/// <inheritdoc />
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
