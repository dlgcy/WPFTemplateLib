using System;
using System.ComponentModel;

namespace WPFTemplateLib.Utilities
{
	/// <summary>
	/// 字符串扩展（来自：HandyControl.Tools.Extension）
	/// </summary>
	public static class StringExtension
	{
		public static T Value<T>(this string input)
		{
			try
			{
				return (T) TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(input);
			}
			catch
			{
				return default;
			}
		}

		public static object Value(this string input, Type type)
		{
			try
			{
				return TypeDescriptor.GetConverter(type).ConvertFromString(input);
			}
			catch
			{
				return null;
			}
		}
	}
}
