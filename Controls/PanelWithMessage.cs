﻿using System.Windows;
using System.Windows.Controls;

namespace WPFTemplateLib.Controls
{
	/// <summary>
	/// [自定义控件] 带信息显示区的面板控件（VM 中需要有名为“Info”的字符串类型的属性，通过该属性输入新消息信息）
	/// </summary>
	public class PanelWithMessage : ContentControl
	{
		static PanelWithMessage()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(PanelWithMessage), new FrameworkPropertyMetadata(typeof(PanelWithMessage)));
		}

		#region 依赖属性

		/// <summary>
		/// [依赖属性] 内容区域大小（默认为 3*）
		/// </summary>
		public GridLength ContentRegionLength
		{
			get => (GridLength)GetValue(ContentRegionLengthProperty);
			set => SetValue(ContentRegionLengthProperty, value);
		}
		public static readonly DependencyProperty ContentRegionLengthProperty =
			DependencyProperty.Register(nameof(ContentRegionLength), typeof(GridLength), typeof(PanelWithMessage), new PropertyMetadata(new GridLength(3, GridUnitType.Star)));

		/// <summary>
		/// [依赖属性]消息区域大小
		/// </summary>
		public GridLength MessageRegionLength
		{
			get => (GridLength)GetValue(MessageRegionLengthProperty);
			set => SetValue(MessageRegionLengthProperty, value);
		}
		public static readonly DependencyProperty MessageRegionLengthProperty =
			DependencyProperty.Register(nameof(MessageRegionLength), typeof(GridLength), typeof(PanelWithMessage), new PropertyMetadata(new GridLength(1, GridUnitType.Star)));

		#endregion
	}
}
