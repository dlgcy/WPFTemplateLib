namespace WPFTemplateLib.Const
{
	/// <summary>
	/// 常用值
	/// </summary>
	public static class CommonValues
	{
		/// <summary>
		/// true 值
		/// </summary>
		public const bool TrueValue = true;

		/// <summary>
		/// false 值
		/// </summary>
		public const bool FalseValue = false;
	}
}
