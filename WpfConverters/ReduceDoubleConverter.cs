using System;
using System.Globalization;
using System.Windows.Data;
using WPFTemplateLib.WpfConverters.Core;

/*
 * 源码已托管：https://gitee.com/dlgcy/WPFTemplate
 */
namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// 减少转换器(参数传要减少的数值，double 类型)
    /// </summary>
    public class ReduceDoubleConverter : ValueConverterBase
    {
		/// <summary>
		/// 是否限制为正数
		/// </summary>
		public bool IsLimitPositive { get; set; }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrWhiteSpace(parameter+""))
                throw new ArgumentNullException(nameof(parameter), "parameter can not be null or empty");

            if (!double.TryParse(parameter + "", out double reduceValue))
                throw new ArgumentException("parameter must be double type", nameof(parameter));

            if (!double.TryParse(value + "", out double originValue))
                throw new ArgumentException("value must can be convert to double type", nameof(value));

			double result = originValue - reduceValue;
			if(IsLimitPositive && result < 0)
			{
				result = 0;
			}

			return result;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
    }
}
