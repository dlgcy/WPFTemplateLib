using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
/*
 * 源码已托管：https://gitee.com/dlgcy/WPFTemplate
 */
namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// [多值转换器] 判断给定的数据的字符串形式是否都相等
    /// </summary>
    public class IsAllStrEqualMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
			try
			{
				if (values == null || values.Length == 0)
					throw new ArgumentNullException(nameof(values), "values can not be null or empty");

				return values.All(x => x + "" == values[0] + "");
			}

			catch (Exception ex)
			{
				Console.WriteLine(ex);
				Debug.WriteLine($"{ex}");
				return false;
			}
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
			throw new NotImplementedException();
		}
    }
}
