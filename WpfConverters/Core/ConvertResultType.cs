namespace WPFTemplateLib.WpfConverters.Core
{
	/// <summary>
	/// 转换结果的类型
	/// </summary>
	public enum ConvertResultType
	{
		Visibility = 1,
		Bool = 2,
		String = 3,
		Int = 4,
		Float = 5,
		Double = 6,
		SolidColorBrush = 7,
		UInt = 8,
		Short = 9,
		UShort = 10,
		Byte = 11,
		Long = 12,
	}
}
