using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace WPFTemplateLib.TypeConverters
{
	/// <summary>
	/// 字符串转字符数组
	/// </summary>
	public class StringToCharArrayTypeConveter : TypeConverter
	{
		/// <inheritdoc/>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			return (value + "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
		}

		/// <inheritdoc/>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(char[]);
		}

		/// <inheritdoc/>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string);
		}

		/// <inheritdoc/>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			var arr = (value + "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => Convert.ToChar((string)s)).ToArray();
			return arr;
		}
	}
}