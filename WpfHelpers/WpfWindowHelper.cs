using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Xaml.Behaviors.Layout;
using System.Windows.Media;
using System.Windows.Documents;

namespace WPFTemplateLib.WpfHelpers
{
	/// <summary>
	/// WPF 窗口帮助类
	/// </summary>
	public static class WpfWindowHelper
	{
		/// <summary>
		/// [dlgcy] 获取激活的窗口
		/// </summary>
		/// <returns>激活的窗口，没有则为null</returns>
		public static Window GetActiveWindow()
		{
			int windowsCount = Application.Current.Windows.Count;
			Window[] windows = new Window[windowsCount];
			Application.Current.Windows.CopyTo(windows, 0);
			Window activeWindow = windows.FirstOrDefault(x => x.IsActive);
			return activeWindow;
		}

		#region 蒙板弹窗

		/// <summary>
		/// 弹窗时带蒙板
		/// </summary>
		/// <param name="win">弹窗</param>
		/// <param name="maskColorStr">蒙板颜色字符串</param>
		public static void ShowWithMask(this Window win, string maskColorStr = "#88000000")
		{
			AddMask(win, maskColorStr);
			win.Show();
		}

		/// <summary>
		/// 模态弹窗时带蒙板
		/// </summary>
		/// <param name="win">弹窗</param>
		/// <param name="maskColorStr">蒙板颜色字符串</param>
		/// <returns>模态弹窗结果</returns>
		public static bool? ShowDialogWithMask(this Window win, string maskColorStr = "#88000000")
		{
			AddMask(win, maskColorStr);
			return win.ShowDialog();
		}

		/// <summary>
		/// 给弹窗后面加上蒙板
		/// </summary>
		/// <param name="showUpWin">要弹出的弹窗</param>
		/// <param name="maskColorStr">蒙板颜色字符串</param>
		public static void AddMask(this Window showUpWin, string maskColorStr = "#88000000")
		{
			Visual parent = GetActiveWindow();
			if(parent == null)
				return;

			var layer = AdornerLayer.GetAdornerLayer(parent);
			if(layer == null)
				return;

			var adornerContainer = new AdornerContainer(layer)
			{
				Child = new Border() { Background = new SolidColorBrush(MediaColorHelper.ColorStrToMediaColor(maskColorStr)) }
			};
			layer.Add(adornerContainer);

			if(showUpWin != null)
			{
				showUpWin.Closed += delegate
				{
					layer.Remove(adornerContainer);
				};
			}
		}

		#endregion
	}
}
