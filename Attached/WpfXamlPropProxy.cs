using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPFTemplateLib.Enums;

namespace WPFTemplateLib.Attached
{
	/// <summary>
	/// WPF Xaml 元素属性代理（用于设置属性以便在控件模板等地方使用）
	/// </summary>
	/// <remarks>
	///	文章：https://dlgcy.com/use-attached-dependency-property-to-reduce-style-code/
	/// </remarks>
	public class WpfXamlPropProxy
	{
		#region 圆角
		public static CornerRadius GetCornerRadius(DependencyObject obj)
		{
			return (CornerRadius)obj.GetValue(CornerRadiusProperty);
		}

		public static void SetCornerRadius(DependencyObject obj, CornerRadius value)
		{
			obj.SetValue(CornerRadiusProperty, value);
		}

		/// <summary>
		/// 圆角
		/// </summary>
		public static readonly DependencyProperty CornerRadiusProperty =
			DependencyProperty.RegisterAttached("CornerRadius", typeof(CornerRadius), typeof(WpfXamlPropProxy), new PropertyMetadata(new CornerRadius()));
		#endregion

		#region 边框粗细
		public static Thickness GetBorderThickness(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(BorderThicknessProperty);
		}

		public static void SetBorderThickness(DependencyObject obj, Thickness value)
		{
			obj.SetValue(BorderThicknessProperty, value);
		}

		/// <summary>
		/// 边框粗细
		/// </summary>
		public static readonly DependencyProperty BorderThicknessProperty =
			DependencyProperty.RegisterAttached("BorderThickness", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness()));
		#endregion

		#region 边框画刷
		public static Brush GetBorderBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(BorderBrushProperty);
		}

		public static void SetBorderBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(BorderBrushProperty, value);
		}

		/// <summary>
		/// 边框画刷
		/// </summary>
		public static readonly DependencyProperty BorderBrushProperty =
			DependencyProperty.RegisterAttached("BorderBrush", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 鼠标移上的背景画刷
		public static Brush GetMouseOverBackground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(MouseOverBackgroundProperty);
		}

		public static void SetMouseOverBackground(DependencyObject obj, Brush value)
		{
			obj.SetValue(MouseOverBackgroundProperty, value);
		}

		/// <summary>
		/// 鼠标移上的背景画刷
		/// </summary>
		public static readonly DependencyProperty MouseOverBackgroundProperty =
			DependencyProperty.RegisterAttached("MouseOverBackground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 鼠标移上的前景画刷
		public static Brush GetMouseOverForeground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(MouseOverForegroundProperty);
		}

		public static void SetMouseOverForeground(DependencyObject obj, Brush value)
		{
			obj.SetValue(MouseOverForegroundProperty, value);
		}

		/// <summary>
		/// 鼠标移上的前景画刷
		/// </summary>
		public static readonly DependencyProperty MouseOverForegroundProperty =
			DependencyProperty.RegisterAttached("MouseOverForeground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 鼠标移上的边框画刷
		public static Brush GetMouseOverBorderBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(MouseOverBorderBrushProperty);
		}

		public static void SetMouseOverBorderBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(MouseOverBorderBrushProperty, value);
		}

		/// <summary>
		/// 鼠标移上的边框画刷
		/// </summary>
		public static readonly DependencyProperty MouseOverBorderBrushProperty =
			DependencyProperty.RegisterAttached("MouseOverBorderBrush", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 鼠标移上的边框粗细(默认1)
		public static Thickness GetMouseOverBorderThickness(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(MouseOverBorderThicknessProperty);
		}
		/// <summary>
		/// 设置 鼠标移上的边框粗细(默认1)
		/// </summary>
		public static void SetMouseOverBorderThickness(DependencyObject obj, Thickness value)
		{
			obj.SetValue(MouseOverBorderThicknessProperty, value);
		}
		/// <summary>
		/// [附加属性] 鼠标移上的边框粗细(默认1)
		/// </summary>
		public static readonly DependencyProperty MouseOverBorderThicknessProperty =
			DependencyProperty.RegisterAttached("MouseOverBorderThickness", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(1)));
		#endregion

		#region 鼠标按下的背景画刷
		public static Brush GetPressedBackground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(PressedBackgroundProperty);
		}

		public static void SetPressedBackground(DependencyObject obj, Brush value)
		{
			obj.SetValue(PressedBackgroundProperty, value);
		}

		/// <summary>
		/// 鼠标按下的背景画刷
		/// </summary>
		public static readonly DependencyProperty PressedBackgroundProperty =
			DependencyProperty.RegisterAttached("PressedBackground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 鼠标按下的前景画刷
		public static Brush GetPressedForeground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(PressedForegroundProperty);
		}

		public static void SetPressedForeground(DependencyObject obj, Brush value)
		{
			obj.SetValue(PressedForegroundProperty, value);
		}

		/// <summary>
		/// 鼠标按下的前景画刷
		/// </summary>
		public static readonly DependencyProperty PressedForegroundProperty =
			DependencyProperty.RegisterAttached("PressedForeground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 鼠标按下的边框画刷
		public static Brush GetPressedBorderBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(PressedBorderBrushProperty);
		}

		public static void SetPressedBorderBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(PressedBorderBrushProperty, value);
		}

		/// <summary>
		/// 鼠标按下的边框画刷
		/// </summary>
		public static readonly DependencyProperty PressedBorderBrushProperty =
			DependencyProperty.RegisterAttached("PressedBorderBrush", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 鼠标按下的边框粗细(默认1)
		public static Thickness GetPressedBorderThickness(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(PressedBorderThicknessProperty);
		}
		/// <summary>
		/// 设置 鼠标按下的边框粗细(默认1)
		/// </summary>
		public static void SetPressedBorderThickness(DependencyObject obj, Thickness value)
		{
			obj.SetValue(PressedBorderThicknessProperty, value);
		}
		/// <summary>
		/// [附加属性] 鼠标按下的边框粗细(默认1)
		/// </summary>
		public static readonly DependencyProperty PressedBorderThicknessProperty =
			DependencyProperty.RegisterAttached("PressedBorderThickness", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(1)));
		#endregion

		#region [附加属性]鼠标按下时显示出来的背后的颜色
		public static Brush GetPressedBackgroundBehind(DependencyObject obj)
		{
			return (Brush)obj.GetValue(PressedBackgroundBehindProperty);
		}
		public static void SetPressedBackgroundBehind(DependencyObject obj, Brush value)
		{
			obj.SetValue(PressedBackgroundBehindProperty, value);
		}
		/// <summary>
		/// [附加属性]鼠标按下时显示出来的背后的颜色
		/// </summary>
		public static readonly DependencyProperty PressedBackgroundBehindProperty =
			DependencyProperty.RegisterAttached("PressedBackgroundBehind", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]鼠标按下时内容收缩的边距
		public static Thickness GetPressedContentMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(PressedContentMarginProperty);
		}
		public static void SetPressedContentMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(PressedContentMarginProperty, value);
		}
		/// <summary>
		/// [附加属性]鼠标按下时内容收缩的边距
		/// </summary>
		public static readonly DependencyProperty PressedContentMarginProperty =
			DependencyProperty.RegisterAttached("PressedContentMargin", typeof(Thickness), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]鼠标按下时出现的阴影颜色
		public static SolidColorBrush GetPressedShadowBrush(DependencyObject obj)
		{
			return (SolidColorBrush)obj.GetValue(PressedShadowBrushProperty);
		}
		public static void SetPressedShadowBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(PressedShadowBrushProperty, value);
		}
		/// <summary>
		/// [附加属性]鼠标按下时出现的阴影颜色
		/// </summary>
		public static readonly DependencyProperty PressedShadowBrushProperty =
			DependencyProperty.RegisterAttached("PressedShadowBrush", typeof(SolidColorBrush), typeof(WpfXamlPropProxy));
		#endregion

		#region 禁用时的的背景画刷
		public static Brush GetDisabledBackground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(DisabledBackgroundProperty);
		}

		public static void SetDisabledBackground(DependencyObject obj, Brush value)
		{
			obj.SetValue(DisabledBackgroundProperty, value);
		}

		/// <summary>
		/// 禁用时的的背景画刷
		/// </summary>
		public static readonly DependencyProperty DisabledBackgroundProperty =
			DependencyProperty.RegisterAttached("DisabledBackground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 禁用时的的前景画刷
		public static Brush GetDisabledForeground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(DisabledForegroundProperty);
		}

		public static void SetDisabledForeground(DependencyObject obj, Brush value)
		{
			obj.SetValue(DisabledForegroundProperty, value);
		}

		/// <summary>
		/// 禁用时的的前景画刷
		/// </summary>
		public static readonly DependencyProperty DisabledForegroundProperty =
			DependencyProperty.RegisterAttached("DisabledForeground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region 禁用时的的边框画刷
		public static Brush GetDisabledBorderBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(DisabledBorderBrushProperty);
		}

		public static void SetDisabledBorderBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(DisabledBorderBrushProperty, value);
		}

		/// <summary>
		/// 禁用时的的边框画刷
		/// </summary>
		public static readonly DependencyProperty DisabledBorderBrushProperty =
			DependencyProperty.RegisterAttached("DisabledBorderBrush", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]被选中时的背景画刷
		public static Brush GetSelectedBackground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(SelectedBackgroundProperty);
		}
		public static void SetSelectedBackground(DependencyObject obj, Brush value)
		{
			obj.SetValue(SelectedBackgroundProperty, value);
		}
		/// <summary>
		/// [附加属性]被选中时的背景画刷
		/// </summary>
		public static readonly DependencyProperty SelectedBackgroundProperty =
			DependencyProperty.RegisterAttached("SelectedBackground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]被选中时的前景画刷
		public static Brush GetSelectedForeground(DependencyObject obj)
		{
			return (Brush)obj.GetValue(SelectedForegroundProperty);
		}
		public static void SetSelectedForeground(DependencyObject obj, Brush value)
		{
			obj.SetValue(SelectedForegroundProperty, value);
		}
		/// <summary>
		/// [附加属性]被选中时的前景画刷
		/// </summary>
		public static readonly DependencyProperty SelectedForegroundProperty =
			DependencyProperty.RegisterAttached("SelectedForeground", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]被选中时的边框画刷
		public static Brush GetSelectedBorderBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(SelectedBorderBrushProperty);
		}
		public static void SetSelectedBorderBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(SelectedBorderBrushProperty, value);
		}
		/// <summary>
		/// [附加属性]被选中时的边框画刷
		/// </summary>
		public static readonly DependencyProperty SelectedBorderBrushProperty =
			DependencyProperty.RegisterAttached("SelectedBorderBrush", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 被选中时的边框粗细(默认1)
		public static Thickness GetSelectedBorderThickness(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(SelectedBorderThicknessProperty);
		}
		/// <summary>
		/// 设置 被选中时的边框粗细(默认1)
		/// </summary>
		public static void SetSelectedBorderThickness(DependencyObject obj, Thickness value)
		{
			obj.SetValue(SelectedBorderThicknessProperty, value);
		}
		/// <summary>
		/// [附加属性] 被选中时的边框粗细(默认1)
		/// </summary>
		public static readonly DependencyProperty SelectedBorderThicknessProperty =
			DependencyProperty.RegisterAttached("SelectedBorderThickness", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(1)));
		#endregion

		#region [附加属性]Path绘图时的Data数据
		public static Geometry GetPathData(DependencyObject obj)
		{
			return (Geometry)obj.GetValue(PathDataProperty);
		}
		public static void SetPathData(DependencyObject obj, Geometry value)
		{
			obj.SetValue(PathDataProperty, value);
		}
		/// <summary>
		/// [附加属性]Path绘图时的Data数据
		/// </summary>
		public static readonly DependencyProperty PathDataProperty =
			DependencyProperty.RegisterAttached("PathData", typeof(Geometry), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]Image Uri 字符串
		public static string GetImageUriStr(DependencyObject obj)
		{
			return (string)obj.GetValue(ImageUriStrProperty);
		}
		public static void SetImageUriStr(DependencyObject obj, string value)
		{
			obj.SetValue(ImageUriStrProperty, value);
		}
		/// <summary>
		/// [附加属性]Image Uri 字符串
		/// </summary>
		public static readonly DependencyProperty ImageUriStrProperty =
			DependencyProperty.RegisterAttached("ImageUriStr", typeof(string), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]Image Uri
		public static Uri GetImageUri(DependencyObject obj)
		{
			return (Uri)obj.GetValue(ImageUriProperty);
		}
		public static void SetImageUri(DependencyObject obj, Uri value)
		{
			obj.SetValue(ImageUriProperty, value);
		}
		/// <summary>
		/// [附加属性]Image Uri
		/// </summary>
		public static readonly DependencyProperty ImageUriProperty =
			DependencyProperty.RegisterAttached("ImageUri", typeof(Uri), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 被选中时的图片 Uri
		public static Uri GetSelectedImageUri(DependencyObject obj)
		{
			return (Uri)obj.GetValue(SelectedImageUriProperty);
		}
		/// <summary>
		/// 被选中时的图片 Uri
		/// </summary>
		public static void SetSelectedImageUri(DependencyObject obj, Uri value)
		{
			obj.SetValue(SelectedImageUriProperty, value);
		}
		/// <summary>
		/// [附加属性] 被选中时的图片 Uri
		/// </summary>
		public static readonly DependencyProperty SelectedImageUriProperty =
			DependencyProperty.RegisterAttached("SelectedImageUri", typeof(Uri), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 鼠标悬停时的图片 Uri
		public static Uri GetMouseOverImageUri(DependencyObject obj)
		{
			return (Uri)obj.GetValue(MouseOverImageUriProperty);
		}
		/// <summary>
		/// 鼠标悬停时的图片 Uri
		/// </summary>
		public static void SetMouseOverImageUri(DependencyObject obj, Uri value)
		{
			obj.SetValue(MouseOverImageUriProperty, value);
		}
		/// <summary>
		/// [附加属性] 鼠标悬停时的图片 Uri
		/// </summary>
		public static readonly DependencyProperty MouseOverImageUriProperty =
			DependencyProperty.RegisterAttached("MouseOverImageUri", typeof(Uri), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性]滚动条的宽或高
		public static double GetScrollBarWidthOrHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(ScrollBarWidthOrHeightProperty);
		}
		public static void SetScrollBarWidthOrHeight(DependencyObject obj, double value)
		{
			obj.SetValue(ScrollBarWidthOrHeightProperty, value);
		}
		/// <summary>
		/// [附加属性]滚动条的宽或高
		/// </summary>
		public static readonly DependencyProperty ScrollBarWidthOrHeightProperty =
			DependencyProperty.RegisterAttached("ScrollBarWidthOrHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(8d));
		#endregion

		#region [附加属性]ID
		public static string GetId(DependencyObject obj)
		{
			return (string)obj.GetValue(IdProperty);
		}
		public static void SetId(DependencyObject obj, string value)
		{
			obj.SetValue(IdProperty, value);
		}
		/// <summary>
		/// [附加属性]ID
		/// </summary>
		public static readonly DependencyProperty IdProperty =
			DependencyProperty.RegisterAttached("Id", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata("-1"));
		#endregion

		#region [附加属性]当前选择ID
		public static string GetCurrentSelectedId(DependencyObject obj)
		{
			return (string)obj.GetValue(CurrentSelectedIdProperty);
		}
		public static void SetCurrentSelectedId(DependencyObject obj, string value)
		{
			obj.SetValue(CurrentSelectedIdProperty, value);
		}
		/// <summary>
		/// [附加属性]当前选择ID
		/// </summary>
		public static readonly DependencyProperty CurrentSelectedIdProperty =
			DependencyProperty.RegisterAttached("CurrentSelectedId", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata(""));
		#endregion

		#region [附加属性]文字
		public static string GetText(DependencyObject obj)
		{
			return (string)obj.GetValue(TextProperty);
		}
		public static void SetText(DependencyObject obj, string value)
		{
			obj.SetValue(TextProperty, value);
		}
		/// <summary>
		/// [附加属性]文字
		/// </summary>
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.RegisterAttached("Text", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata("<默认文字>"));
		#endregion

		#region [附加属性]图片位置
		public static Dock GetImagePosition(DependencyObject obj)
		{
			return (Dock)obj.GetValue(ImagePositionProperty);
		}
		/// <summary>
		/// 图片位置
		/// </summary>
		public static void SetImagePosition(DependencyObject obj, Dock value)
		{
			obj.SetValue(ImagePositionProperty, value);
		}
		/// <summary>
		/// [附加属性]图片位置
		/// </summary>
		public static readonly DependencyProperty ImagePositionProperty =
			DependencyProperty.RegisterAttached("ImagePosition", typeof(Dock), typeof(WpfXamlPropProxy), new PropertyMetadata(Dock.Left));
		#endregion

		#region [附加属性]图片宽度（默认32）
		public static double GetImageWidth(DependencyObject obj)
		{
			return (double)obj.GetValue(ImageWidthProperty);
		}
		/// <summary>
		/// 图片宽度（默认32）
		/// </summary>
		public static void SetImageWidth(DependencyObject obj, double value)
		{
			obj.SetValue(ImageWidthProperty, value);
		}
		/// <summary>
		/// [附加属性]图片宽度（默认32）
		/// </summary>
		public static readonly DependencyProperty ImageWidthProperty =
			DependencyProperty.RegisterAttached("ImageWidth", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(32d));
		#endregion

		#region [附加属性]图片高度（默认32）
		public static double GetImageHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(ImageHeightProperty);
		}
		/// <summary>
		/// 图片高度（默认32）
		/// </summary>
		public static void SetImageHeight(DependencyObject obj, double value)
		{
			obj.SetValue(ImageHeightProperty, value);
		}
		/// <summary>
		/// [附加属性]图片高度（默认32）
		/// </summary>
		public static readonly DependencyProperty ImageHeightProperty =
			DependencyProperty.RegisterAttached("ImageHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(32d));
		#endregion

		#region [附加属性]图片边距（默认5）
		public static Thickness GetImageMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(ImageMarginProperty);
		}
		/// <summary>
		/// 图片边距（默认5）
		/// </summary>
		public static void SetImageMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(ImageMarginProperty, value);
		}
		/// <summary>
		/// [附加属性]图片边距（默认5）
		/// </summary>
		public static readonly DependencyProperty ImageMarginProperty =
			DependencyProperty.RegisterAttached("ImageMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5)));
		#endregion

		#region [附加属性]文字区域边距（默认5）
		public static Thickness GetTextRegionMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(TextRegionMarginProperty);
		}
		/// <summary>
		/// 文字区域边距（默认5）
		/// </summary>
		public static void SetTextRegionMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(TextRegionMarginProperty, value);
		}
		/// <summary>
		/// [附加属性]文字区域边距（默认5）
		/// </summary>
		public static readonly DependencyProperty TextRegionMarginProperty =
			DependencyProperty.RegisterAttached("TextRegionMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5)));
		#endregion

		#region [附加属性]文字对齐方式（默认左对齐）
		public static TextAlignment GetTextAlignment(DependencyObject obj)
		{
			return (TextAlignment)obj.GetValue(TextAlignmentProperty);
		}
		/// <summary>
		/// 文字对齐方式（默认左对齐）
		/// </summary>
		public static void SetTextAlignment(DependencyObject obj, TextAlignment value)
		{
			obj.SetValue(TextAlignmentProperty, value);
		}
		/// <summary>
		/// [附加属性]文字对齐方式（默认左对齐）
		/// </summary>
		public static readonly DependencyProperty TextAlignmentProperty =
			DependencyProperty.RegisterAttached("TextAlignment", typeof(TextAlignment), typeof(WpfXamlPropProxy), new PropertyMetadata(TextAlignment.Left));
		#endregion

		#region [附加属性] 是否是开启状态（默认 False）
		public static bool GetIsOn(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsOnProperty);
		}
		/// <summary>
		/// 是否是开启状态（默认 False）
		/// </summary>
		public static void SetIsOn(DependencyObject obj, bool value)
		{
			obj.SetValue(IsOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 是否是开启状态（默认 False）
		/// </summary>
		public static readonly DependencyProperty IsOnProperty =
			DependencyProperty.RegisterAttached("IsOn", typeof(bool), typeof(WpfXamlPropProxy), new PropertyMetadata(false));
		#endregion

		#region [附加属性] 开启时的颜色
		public static Color GetColorOn(DependencyObject obj)
		{
			return (Color)obj.GetValue(ColorOnProperty);
		}
		/// <summary>
		/// 开启时的颜色
		/// </summary>
		public static void SetColorOn(DependencyObject obj, Color value)
		{
			obj.SetValue(ColorOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 开启时的颜色
		/// </summary>
		public static readonly DependencyProperty ColorOnProperty =
			DependencyProperty.RegisterAttached("ColorOn", typeof(Color), typeof(WpfXamlPropProxy), new PropertyMetadata(Colors.Transparent));
		#endregion

		#region [附加属性] 关闭时的颜色
		public static Color GetColorOff(DependencyObject obj)
		{
			return (Color)obj.GetValue(ColorOffProperty);
		}
		/// <summary>
		/// 关闭时的颜色
		/// </summary>
		public static void SetColorOff(DependencyObject obj, Color value)
		{
			obj.SetValue(ColorOffProperty, value);
		}
		/// <summary>
		/// [附加属性] 关闭时的颜色
		/// </summary>
		public static readonly DependencyProperty ColorOffProperty =
			DependencyProperty.RegisterAttached("ColorOff", typeof(Color), typeof(WpfXamlPropProxy), new PropertyMetadata(Colors.Transparent));
		#endregion

		#region [附加属性] 开启时的颜色画刷
		public static Brush GetBrushWhenOn(DependencyObject obj)
		{
			return (Brush)obj.GetValue(BrushWhenOnProperty);
		}
		/// <summary>
		/// 设置 开启时的颜色画刷
		/// </summary>
		public static void SetBrushWhenOn(DependencyObject obj, Brush value)
		{
			obj.SetValue(BrushWhenOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 开启时的颜色画刷
		/// </summary>
		public static readonly DependencyProperty BrushWhenOnProperty =
			DependencyProperty.RegisterAttached("BrushWhenOn", typeof(Brush), typeof(WpfXamlPropProxy), new PropertyMetadata(new SolidColorBrush(Colors.LimeGreen)));
		#endregion

		#region [附加属性] 关闭时的颜色画刷
		public static Brush GetBrushWhenOff(DependencyObject obj)
		{
			return (Brush)obj.GetValue(BrushWhenOffProperty);
		}
		/// <summary>
		/// 设置 关闭时的颜色画刷
		/// </summary>
		public static void SetBrushWhenOff(DependencyObject obj, Brush value)
		{
			obj.SetValue(BrushWhenOffProperty, value);
		}
		/// <summary>
		/// [附加属性] 关闭时的颜色画刷
		/// </summary>
		public static readonly DependencyProperty BrushWhenOffProperty =
			DependencyProperty.RegisterAttached("BrushWhenOff", typeof(Brush), typeof(WpfXamlPropProxy), new PropertyMetadata(new SolidColorBrush(Colors.OrangeRed)));
		#endregion

		#region [附加属性] 开启时的背景画刷
		public static Brush GetBackgroundWhenOn(DependencyObject obj)
		{
			return (Brush)obj.GetValue(BackgroundWhenOnProperty);
		}
		/// <summary>
		/// 设置 开启时的背景画刷
		/// </summary>
		public static void SetBackgroundWhenOn(DependencyObject obj, Brush value)
		{
			obj.SetValue(BackgroundWhenOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 开启时的背景画刷
		/// </summary>
		public static readonly DependencyProperty BackgroundWhenOnProperty =
			DependencyProperty.RegisterAttached("BackgroundWhenOn", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 开启时的前景画刷
		public static Brush GetForegroundWhenOn(DependencyObject obj)
		{
			return (Brush)obj.GetValue(ForegroundWhenOnProperty);
		}
		/// <summary>
		/// 设置 开启时的前景画刷
		/// </summary>
		public static void SetForegroundWhenOn(DependencyObject obj, Brush value)
		{
			obj.SetValue(ForegroundWhenOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 开启时的前景画刷
		/// </summary>
		public static readonly DependencyProperty ForegroundWhenOnProperty =
			DependencyProperty.RegisterAttached("ForegroundWhenOn", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 开启时的边框画刷
		public static Brush GetBorderBrushWhenOn(DependencyObject obj)
		{
			return (Brush)obj.GetValue(BorderBrushWhenOnProperty);
		}
		/// <summary>
		/// 设置 开启时的边框画刷
		/// </summary>
		public static void SetBorderBrushWhenOn(DependencyObject obj, Brush value)
		{
			obj.SetValue(BorderBrushWhenOnProperty, value);
		}
		/// <summary>
		/// [附加属性] 开启时的边框画刷
		/// </summary>
		public static readonly DependencyProperty BorderBrushWhenOnProperty =
			DependencyProperty.RegisterAttached("BorderBrushWhenOn", typeof(Brush), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 是否启用
		public static bool GetIsEnabled(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsEnabledProperty);
		}
		/// <summary>
		/// 是否启用
		/// </summary>
		public static void SetIsEnabled(DependencyObject obj, bool value)
		{
			obj.SetValue(IsEnabledProperty, value);
		}
		/// <summary>
		/// [附加属性] 是否启用
		/// </summary>
		public static readonly DependencyProperty IsEnabledProperty =
			DependencyProperty.RegisterAttached("IsEnabled", typeof(bool), typeof(WpfXamlPropProxy), new PropertyMetadata(true));
		#endregion

		#region [附加属性] 标题名称
		public static string GetTitleName(DependencyObject obj)
		{
			return (string)obj.GetValue(TitleNameProperty);
		}
		/// <summary>
		/// 设置 标题名称
		/// </summary>
		public static void SetTitleName(DependencyObject obj, string value)
		{
			obj.SetValue(TitleNameProperty, value);
		}
		/// <summary>
		/// [附加属性] 标题名称
		/// </summary>
		public static readonly DependencyProperty TitleNameProperty =
			DependencyProperty.RegisterAttached("TitleName", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata(string.Empty));
		#endregion

		#region [附加属性] 显示值
		public static string GetShowValue(DependencyObject obj)
		{
			return (string)obj.GetValue(ShowValueProperty);
		}
		/// <summary>
		/// 设置 显示值
		/// </summary>
		public static void SetShowValue(DependencyObject obj, string value)
		{
			obj.SetValue(ShowValueProperty, value);
		}
		/// <summary>
		/// [附加属性] 显示值
		/// </summary>
		public static readonly DependencyProperty ShowValueProperty =
			DependencyProperty.RegisterAttached("ShowValue", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata(string.Empty));
		#endregion

		#region [附加属性] 实际值
		public static object GetRealValue(DependencyObject obj)
		{
			return (object)obj.GetValue(RealValueProperty);
		}
		/// <summary>
		/// 设置 实际值
		/// </summary>
		public static void SetRealValue(DependencyObject obj, object value)
		{
			obj.SetValue(RealValueProperty, value);
		}
		/// <summary>
		/// [附加属性] 实际值
		/// </summary>
		public static readonly DependencyProperty RealValueProperty =
			DependencyProperty.RegisterAttached("RealValue", typeof(object), typeof(WpfXamlPropProxy), new PropertyMetadata(null));
		#endregion

		#region [附加属性] 单位
		public static string GetUnit(DependencyObject obj)
		{
			return (string)obj.GetValue(UnitProperty);
		}
		/// <summary>
		/// 设置 单位
		/// </summary>
		public static void SetUnit(DependencyObject obj, string value)
		{
			obj.SetValue(UnitProperty, value);
		}
		/// <summary>
		/// [附加属性] 单位
		/// </summary>
		public static readonly DependencyProperty UnitProperty =
			DependencyProperty.RegisterAttached("Unit", typeof(string), typeof(WpfXamlPropProxy), new PropertyMetadata(string.Empty));
		#endregion

		#region [附加属性] 固定部分宽度（默认32）
		public static double GetFixPartWidth(DependencyObject obj)
		{
			return (double)obj.GetValue(FixPartWidthProperty);
		}
		/// <summary>
		/// 设置 固定部分宽度（默认32）
		/// </summary>
		public static void SetFixPartWidth(DependencyObject obj, double value)
		{
			obj.SetValue(FixPartWidthProperty, value);
		}
		/// <summary>
		/// [附加属性] 固定部分宽度（默认32）
		/// </summary>
		public static readonly DependencyProperty FixPartWidthProperty =
			DependencyProperty.RegisterAttached("FixPartWidth", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(32d));
		#endregion

		#region [附加属性] 固定部分高度（默认32）
		public static double GetFixPartHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(FixPartHeightProperty);
		}
		/// <summary>
		/// 设置 固定部分高度（默认32）
		/// </summary>
		public static void SetFixPartHeight(DependencyObject obj, double value)
		{
			obj.SetValue(FixPartHeightProperty, value);
		}
		/// <summary>
		/// [附加属性] 固定部分高度（默认32）
		/// </summary>
		public static readonly DependencyProperty FixPartHeightProperty =
			DependencyProperty.RegisterAttached("FixPartHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(32d));
		#endregion

		#region [附加属性] 固定部分边距(默认5)
		public static Thickness GetFixPartMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(FixPartMarginProperty);
		}
		/// <summary>
		/// 设置 固定部分边距(默认5)
		/// </summary>
		public static void SetFixPartMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(FixPartMarginProperty, value);
		}
		/// <summary>
		/// [附加属性] 固定部分边距(默认5)
		/// </summary>
		public static readonly DependencyProperty FixPartMarginProperty =
			DependencyProperty.RegisterAttached("FixPartMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5)));
		#endregion

		#region [附加属性] 内容部分边距(默认5)
		public static Thickness GetContentPartMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(ContentPartMarginProperty);
		}
		/// <summary>
		/// 设置 内容部分边距(默认5)
		/// </summary>
		public static void SetContentPartMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(ContentPartMarginProperty, value);
		}
		/// <summary>
		/// [附加属性] 内容部分边距(默认5)
		/// </summary>
		public static readonly DependencyProperty ContentPartMarginProperty =
			DependencyProperty.RegisterAttached("ContentPartMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5)));
		#endregion

		#region [附加属性] 内部边距（默认5）
		public static Thickness GetInnerMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(InnerMarginProperty);
		}
		/// <summary>
		/// 设置 内部边距（默认5）
		/// </summary>
		public static void SetInnerMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(InnerMarginProperty, value);
		}
		/// <summary>
		/// [附加属性] 内部边距（默认5）
		/// </summary>
		public static readonly DependencyProperty InnerMarginProperty =
			DependencyProperty.RegisterAttached("InnerMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5)));
		#endregion

		#region [附加属性] 标题字体大小（默认14）
		public static double GetTitleFontSize(DependencyObject obj)
		{
			return (double)obj.GetValue(TitleFontSizeProperty);
		}
		/// <summary>
		/// 设置 标题字体大小（默认14）
		/// </summary>
		public static void SetTitleFontSize(DependencyObject obj, double value)
		{
			obj.SetValue(TitleFontSizeProperty, value);
		}
		/// <summary>
		/// [附加属性] 标题字体大小（默认14）
		/// </summary>
		public static readonly DependencyProperty TitleFontSizeProperty =
			DependencyProperty.RegisterAttached("TitleFontSize", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(14d));
		#endregion

		#region [附加属性] 内容字体大小（默认12）
		public static double GetContentFontSize(DependencyObject obj)
		{
			return (double)obj.GetValue(ContentFontSizeProperty);
		}
		/// <summary>
		/// 设置 内容字体大小（默认12）
		/// </summary>
		public static void SetContentFontSize(DependencyObject obj, double value)
		{
			obj.SetValue(ContentFontSizeProperty, value);
		}
		/// <summary>
		/// [附加属性] 内容字体大小（默认12）
		/// </summary>
		public static readonly DependencyProperty ContentFontSizeProperty =
			DependencyProperty.RegisterAttached("ContentFontSize", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(12d));
		#endregion

		#region [附加属性] 图标填充颜色（默认为空）
		public static Color? GetIconFillColor(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconFillColorProperty);
		}
		/// <summary>
		/// 设置 图标填充颜色（默认为空）
		/// </summary>
		public static void SetIconFillColor(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconFillColorProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标填充颜色（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconFillColorProperty =
			DependencyProperty.RegisterAttached("IconFillColor", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 图标描边颜色（默认为空）
		public static Color? GetIconStrokeColor(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconStrokeColorProperty);
		}
		/// <summary>
		/// 设置 图标描边颜色（默认为空）
		/// </summary>
		public static void SetIconStrokeColor(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconStrokeColorProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标描边颜色（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconStrokeColorProperty =
			DependencyProperty.RegisterAttached("IconStrokeColor", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 图标颜色画刷
		public static Brush GetIconFillBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(IconFillBrushProperty);
		}
		/// <summary>
		/// 设置 图标颜色画刷
		/// </summary>
		public static void SetIconFillBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(IconFillBrushProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标颜色画刷
		/// </summary>
		public static readonly DependencyProperty IconFillBrushProperty =
			DependencyProperty.RegisterAttached("IconFillBrush", typeof(Brush), typeof(WpfXamlPropProxy), new PropertyMetadata(new SolidColorBrush(Colors.DeepSkyBlue)));
		#endregion

		#region [附加属性] 图标描边画刷
		public static Brush GetIconStrokeBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(IconStrokeBrushProperty);
		}
		/// <summary>
		/// 设置 图标描边画刷
		/// </summary>
		public static void SetIconStrokeBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(IconStrokeBrushProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标描边画刷
		/// </summary>
		public static readonly DependencyProperty IconStrokeBrushProperty =
			DependencyProperty.RegisterAttached("IconStrokeBrush", typeof(Brush), typeof(WpfXamlPropProxy), new PropertyMetadata(new SolidColorBrush(Colors.DodgerBlue)));
		#endregion

		#region [附加属性] 图标填充颜色(鼠标移上)（默认为空）
		public static Color? GetIconFillColorMouseOver(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconFillColorMouseOverProperty);
		}
		/// <summary>
		/// 设置 图标填充颜色(鼠标移上)（默认为空）
		/// </summary>
		public static void SetIconFillColorMouseOver(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconFillColorMouseOverProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标填充颜色(鼠标移上)（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconFillColorMouseOverProperty =
			DependencyProperty.RegisterAttached("IconFillColorMouseOver", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 图标描边颜色(鼠标移上)（默认为空）
		public static Color? GetIconStrokeColorMouseOver(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconStrokeColorMouseOverProperty);
		}
		/// <summary>
		/// 设置 图标描边颜色(鼠标移上)（默认为空）
		/// </summary>
		public static void SetIconStrokeColorMouseOver(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconStrokeColorMouseOverProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标描边颜色(鼠标移上)（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconStrokeColorMouseOverProperty =
			DependencyProperty.RegisterAttached("IconStrokeColorMouseOver", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 图标填充颜色(选中)（默认为空）
		public static Color? GetIconFillColorSelected(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconFillColorSelectedProperty);
		}
		/// <summary>
		/// 设置 图标填充颜色(选中)（默认为空）
		/// </summary>
		public static void SetIconFillColorSelected(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconFillColorSelectedProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标填充颜色(选中)（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconFillColorSelectedProperty =
			DependencyProperty.RegisterAttached("IconFillColorSelected", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 图标描边颜色(选中)（默认为空）
		public static Color? GetIconStrokeColorSelected(DependencyObject obj)
		{
			return (Color?)obj.GetValue(IconStrokeColorSelectedProperty);
		}
		/// <summary>
		/// 设置 图标描边颜色(选中)（默认为空）
		/// </summary>
		public static void SetIconStrokeColorSelected(DependencyObject obj, Color? value)
		{
			obj.SetValue(IconStrokeColorSelectedProperty, value);
		}
		/// <summary>
		/// [附加属性] 图标描边颜色(选中)（默认为空）
		/// </summary>
		public static readonly DependencyProperty IconStrokeColorSelectedProperty =
			DependencyProperty.RegisterAttached("IconStrokeColorSelected", typeof(Color?), typeof(WpfXamlPropProxy));
		#endregion

		#region [附加属性] 外部圆圈直径（默认24）
		public static double GetOuterCircleDiameter(DependencyObject obj)
		{
			return (double)obj.GetValue(OuterCircleDiameterProperty);
		}
		/// <summary>
		/// 设置 外部圆圈直径（默认24）
		/// </summary>
		public static void SetOuterCircleDiameter(DependencyObject obj, double value)
		{
			obj.SetValue(OuterCircleDiameterProperty, value);
		}
		/// <summary>
		/// [附加属性] 外部圆圈直径（默认24）
		/// </summary>
		public static readonly DependencyProperty OuterCircleDiameterProperty =
			DependencyProperty.RegisterAttached("OuterCircleDiameter", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(24d));
		#endregion

		#region [附加属性] 内部圆圈直径（默认18）
		public static double GetInnerCircleDiameter(DependencyObject obj)
		{
			return (double)obj.GetValue(InnerCircleDiameterProperty);
		}
		/// <summary>
		/// 设置 内部圆圈直径（默认18）
		/// </summary>
		public static void SetInnerCircleDiameter(DependencyObject obj, double value)
		{
			obj.SetValue(InnerCircleDiameterProperty, value);
		}
		/// <summary>
		/// [附加属性] 内部圆圈直径（默认18）
		/// </summary>
		public static readonly DependencyProperty InnerCircleDiameterProperty =
			DependencyProperty.RegisterAttached("InnerCircleDiameter", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(18d));
		#endregion

		#region [附加属性] 是否可点击
		public static bool GetIsHitTestVisible(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsHitTestVisibleProperty);
		}
		/// <summary>
		/// 是否可点击
		/// </summary>
		public static void SetIsHitTestVisible(DependencyObject obj, bool value)
		{
			obj.SetValue(IsHitTestVisibleProperty, value);
		}
		/// <summary>
		/// [附加属性] 是否可点击
		/// </summary>
		public static readonly DependencyProperty IsHitTestVisibleProperty =
			DependencyProperty.RegisterAttached("IsHitTestVisible", typeof(bool), typeof(WpfXamlPropProxy), new PropertyMetadata(true));
		#endregion

		#region [附加属性] 禁用时的透明度（默认0.4）
		public static double GetOpacityWhenDisable(DependencyObject obj)
		{
			return (double)obj.GetValue(OpacityWhenDisableProperty);
		}
		/// <summary>
		/// 设置 禁用时的透明度（默认0.4）
		/// </summary>
		public static void SetOpacityWhenDisable(DependencyObject obj, double value)
		{
			obj.SetValue(OpacityWhenDisableProperty, value);
		}
		/// <summary>
		/// [附加属性] 禁用时的透明度（默认0.4）
		/// </summary>
		public static readonly DependencyProperty OpacityWhenDisableProperty =
			DependencyProperty.RegisterAttached("OpacityWhenDisable", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(0.4d));
		#endregion

		#region [附加属性] 左侧图标Uri
		public static Uri GetLeftIconUri(DependencyObject obj)
		{
			return (Uri)obj.GetValue(LeftIconUriProperty);
		}
		/// <summary>
		/// 设置 左侧图标Uri
		/// </summary>
		public static void SetLeftIconUri(DependencyObject obj, Uri value)
		{
			obj.SetValue(LeftIconUriProperty, value);
		}
		/// <summary>
		/// [附加属性] 左侧图标Uri
		/// </summary>
		public static readonly DependencyProperty LeftIconUriProperty =
			DependencyProperty.RegisterAttached("LeftIconUri", typeof(Uri), typeof(WpfXamlPropProxy), new PropertyMetadata(null));
		#endregion

		#region [附加属性] 右侧图标Uri
		public static Uri GetRightIconUri(DependencyObject obj)
		{
			return (Uri)obj.GetValue(RightIconUriProperty);
		}
		/// <summary>
		/// 设置 右侧图标Uri
		/// </summary>
		public static void SetRightIconUri(DependencyObject obj, Uri value)
		{
			obj.SetValue(RightIconUriProperty, value);
		}
		/// <summary>
		/// [附加属性] 右侧图标Uri
		/// </summary>
		public static readonly DependencyProperty RightIconUriProperty =
			DependencyProperty.RegisterAttached("RightIconUri", typeof(Uri), typeof(WpfXamlPropProxy), new PropertyMetadata(null));
		#endregion

		#region [附加属性] 左侧图标边距（默认 5,0）
		public static Thickness GetLeftIconMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(LeftIconMarginProperty);
		}
		/// <summary>
		/// 设置 左侧图标边距（默认 5,0）
		/// </summary>
		public static void SetLeftIconMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(LeftIconMarginProperty, value);
		}
		/// <summary>
		/// [附加属性] 左侧图标边距（默认 5,0）
		/// </summary>
		public static readonly DependencyProperty LeftIconMarginProperty =
			DependencyProperty.RegisterAttached("LeftIconMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5, 0, 5, 0)));
		#endregion

		#region [附加属性] 右侧图标边距（默认 5,0）
		public static Thickness GetRightIconMargin(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(RightIconMarginProperty);
		}
		/// <summary>
		/// 设置 右侧图标边距（默认 5,0）
		/// </summary>
		public static void SetRightIconMargin(DependencyObject obj, Thickness value)
		{
			obj.SetValue(RightIconMarginProperty, value);
		}
		/// <summary>
		/// [附加属性] 右侧图标边距（默认 5,0）
		/// </summary>
		public static readonly DependencyProperty RightIconMarginProperty =
			DependencyProperty.RegisterAttached("RightIconMargin", typeof(Thickness), typeof(WpfXamlPropProxy), new PropertyMetadata(new Thickness(5, 0, 5, 0)));
		#endregion

		#region [附加属性] 左侧图标宽度（默认12）
		public static double GetLeftIconWidth(DependencyObject obj)
		{
			return (double)obj.GetValue(LeftIconWidthProperty);
		}
		/// <summary>
		/// 设置 左侧图标宽度（默认12）
		/// </summary>
		public static void SetLeftIconWidth(DependencyObject obj, double value)
		{
			obj.SetValue(LeftIconWidthProperty, value);
		}
		/// <summary>
		/// [附加属性] 左侧图标宽度（默认12）
		/// </summary>
		public static readonly DependencyProperty LeftIconWidthProperty =
			DependencyProperty.RegisterAttached("LeftIconWidth", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(12d));
		#endregion

		#region [附加属性] 左侧图标高度（默认12）
		public static double GetLeftIconHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(LeftIconHeightProperty);
		}
		/// <summary>
		/// 设置 左侧图标高度（默认12）
		/// </summary>
		public static void SetLeftIconHeight(DependencyObject obj, double value)
		{
			obj.SetValue(LeftIconHeightProperty, value);
		}
		/// <summary>
		/// [附加属性] 左侧图标高度（默认12）
		/// </summary>
		public static readonly DependencyProperty LeftIconHeightProperty =
			DependencyProperty.RegisterAttached("LeftIconHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(12d));
		#endregion

		#region [附加属性] 右侧图标宽度（默认12）
		public static double GetRightIconWidth(DependencyObject obj)
		{
			return (double)obj.GetValue(RightIconWidthProperty);
		}
		/// <summary>
		/// 设置 右侧图标宽度（默认12）
		/// </summary>
		public static void SetRightIconWidth(DependencyObject obj, double value)
		{
			obj.SetValue(RightIconWidthProperty, value);
		}
		/// <summary>
		/// [附加属性] 右侧图标宽度（默认12）
		/// </summary>
		public static readonly DependencyProperty RightIconWidthProperty =
			DependencyProperty.RegisterAttached("RightIconWidth", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(12d));
		#endregion

		#region [附加属性] 右侧图标高度（默认12）
		public static double GetRightIconHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(RightIconHeightProperty);
		}
		/// <summary>
		/// 设置 右侧图标高度（默认12）
		/// </summary>
		public static void SetRightIconHeight(DependencyObject obj, double value)
		{
			obj.SetValue(RightIconHeightProperty, value);
		}
		/// <summary>
		/// [附加属性] 右侧图标高度（默认12）
		/// </summary>
		public static readonly DependencyProperty RightIconHeightProperty =
			DependencyProperty.RegisterAttached("RightIconHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(12d));
		#endregion

		#region [附加属性] 方向（默认水平）
		public static Orientation GetOrientation(DependencyObject obj)
		{
			return (Orientation)obj.GetValue(OrientationProperty);
		}
		/// <summary>
		/// 设置 方向（默认水平）
		/// </summary>
		public static void SetOrientation(DependencyObject obj, Orientation value)
		{
			obj.SetValue(OrientationProperty, value);
		}
		/// <summary>
		/// [附加属性] 方向（默认水平）
		/// </summary>
		public static readonly DependencyProperty OrientationProperty =
			DependencyProperty.RegisterAttached("Orientation", typeof(Orientation), typeof(WpfXamlPropProxy), new FrameworkPropertyMetadata(Orientation.Horizontal));
		#endregion

		#region [附加属性] 元素是否换行（默认不换行）
		public static VisualWrapping GetChildWrapping(DependencyObject obj)
		{
			return (VisualWrapping)obj.GetValue(ChildWrappingProperty);
		}
		/// <summary>
		/// 设置 元素是否换行（默认不换行）
		/// </summary>
		public static void SetChildWrapping(DependencyObject obj, VisualWrapping value)
		{
			obj.SetValue(ChildWrappingProperty, value);
		}
		/// <summary>
		/// [附加属性] 元素是否换行（默认不换行）
		/// </summary>
		public static readonly DependencyProperty ChildWrappingProperty =
			DependencyProperty.RegisterAttached("ChildWrapping", typeof(VisualWrapping), typeof(WpfXamlPropProxy), new PropertyMetadata(VisualWrapping.NoWrap));
		#endregion

		#region [附加属性] 空间间隔（默认 double.NaN）
		public static double GetSpacing(DependencyObject obj)
		{
			return (double)obj.GetValue(SpacingProperty);
		}
		/// <summary>
		/// 设置 空间间隔（默认 double.NaN）
		/// </summary>
		public static void SetSpacing(DependencyObject obj, double value)
		{
			obj.SetValue(SpacingProperty, value);
		}
		/// <summary>
		/// [附加属性] 空间间隔（默认 double.NaN）
		/// </summary>
		public static readonly DependencyProperty SpacingProperty =
			DependencyProperty.RegisterAttached("Spacing", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(double.NaN));
		#endregion

		#region [附加属性] 水平空间间隔（默认 double.NaN）
		public static double GetHorizontalSpacing(DependencyObject obj)
		{
			return (double)obj.GetValue(HorizontalSpacingProperty);
		}
		/// <summary>
		/// 设置 水平空间间隔（默认 double.NaN）
		/// </summary>
		public static void SetHorizontalSpacing(DependencyObject obj, double value)
		{
			obj.SetValue(HorizontalSpacingProperty, value);
		}
		/// <summary>
		/// [附加属性] 水平空间间隔（默认 double.NaN）
		/// </summary>
		public static readonly DependencyProperty HorizontalSpacingProperty =
			DependencyProperty.RegisterAttached("HorizontalSpacing", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(double.NaN));
		#endregion

		#region [附加属性] 垂直空间间隔（默认 double.NaN）
		public static double GetVerticalSpacing(DependencyObject obj)
		{
			return (double)obj.GetValue(VerticalSpacingProperty);
		}
		/// <summary>
		/// 设置 垂直空间间隔（默认 double.NaN）
		/// </summary>
		public static void SetVerticalSpacing(DependencyObject obj, double value)
		{
			obj.SetValue(VerticalSpacingProperty, value);
		}
		/// <summary>
		/// [附加属性] 垂直空间间隔（默认 double.NaN）
		/// </summary>
		public static readonly DependencyProperty VerticalSpacingProperty =
			DependencyProperty.RegisterAttached("VerticalSpacing", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(double.NaN));
		#endregion

		#region [附加属性] 元素宽度（默认 double.NaN）
		public static double GetItemWidth(DependencyObject obj)
		{
			return (double)obj.GetValue(ItemWidthProperty);
		}
		/// <summary>
		/// 设置 元素宽度（默认 double.NaN）
		/// </summary>
		public static void SetItemWidth(DependencyObject obj, double value)
		{
			obj.SetValue(ItemWidthProperty, value);
		}
		/// <summary>
		/// [附加属性] 元素宽度（默认 double.NaN）
		/// </summary>
		public static readonly DependencyProperty ItemWidthProperty =
			DependencyProperty.RegisterAttached("ItemWidth", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(double.NaN));
		#endregion

		#region [附加属性] 元素高度（默认 double.NaN）
		public static double GetItemHeight(DependencyObject obj)
		{
			return (double)obj.GetValue(ItemHeightProperty);
		}
		/// <summary>
		/// 设置 元素高度（默认 double.NaN）
		/// </summary>
		public static void SetItemHeight(DependencyObject obj, double value)
		{
			obj.SetValue(ItemHeightProperty, value);
		}
		/// <summary>
		/// [附加属性] 元素高度（默认 double.NaN）
		/// </summary>
		public static readonly DependencyProperty ItemHeightProperty =
			DependencyProperty.RegisterAttached("ItemHeight", typeof(double), typeof(WpfXamlPropProxy), new PropertyMetadata(double.NaN));
		#endregion

		#region [附加属性] 元素水平对齐方式（默认伸展）
		public static HorizontalAlignment? GetItemHorizontalAlignment(DependencyObject obj)
		{
			return (HorizontalAlignment?)obj.GetValue(ItemHorizontalAlignmentProperty);
		}
		/// <summary>
		/// 设置 元素水平对齐方式（默认伸展）
		/// </summary>
		public static void SetItemHorizontalAlignment(DependencyObject obj, HorizontalAlignment? value)
		{
			obj.SetValue(ItemHorizontalAlignmentProperty, value);
		}
		/// <summary>
		/// [附加属性] 元素水平对齐方式（默认伸展）
		/// </summary>
		public static readonly DependencyProperty ItemHorizontalAlignmentProperty =
			DependencyProperty.RegisterAttached("ItemHorizontalAlignment", typeof(HorizontalAlignment?), typeof(WpfXamlPropProxy), new PropertyMetadata(HorizontalAlignment.Stretch));
		#endregion

		#region [附加属性] 元素垂直对齐方式（默认伸展）
		public static VerticalAlignment? GetItemVerticalAlignment(DependencyObject obj)
		{
			return (VerticalAlignment?)obj.GetValue(ItemVerticalAlignmentProperty);
		}
		/// <summary>
		/// 设置 元素垂直对齐方式（默认伸展）
		/// </summary>
		public static void SetItemVerticalAlignment(DependencyObject obj, VerticalAlignment? value)
		{
			obj.SetValue(ItemVerticalAlignmentProperty, value);
		}
		/// <summary>
		/// [附加属性] 元素垂直对齐方式（默认伸展）
		/// </summary>
		public static readonly DependencyProperty ItemVerticalAlignmentProperty =
			DependencyProperty.RegisterAttached("ItemVerticalAlignment", typeof(VerticalAlignment?), typeof(WpfXamlPropProxy), new PropertyMetadata(VerticalAlignment.Stretch));
		#endregion
	}
}
