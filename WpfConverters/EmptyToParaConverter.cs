﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// [转换器] 值为空的话转为显示参数指定的值
    /// </summary>
    public class EmptyToParaConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrWhiteSpace(value + ""))
            {
                return parameter;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
