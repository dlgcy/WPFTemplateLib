<UserControl x:Class="WPFTemplateLib.UserControls.CircleWithTextBox"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:local="clr-namespace:WPFTemplateLib.UserControls"
             xmlns:calc="clr-namespace:CalcBinding;assembly=CalcBinding"
             xmlns:markupExtensions="clr-namespace:Mersoft.Mvvm.MarkupExtensions"
             xmlns:xctk="http://schemas.xceed.com/wpf/xaml/toolkit"
             mc:Ignorable="d" d:DesignHeight="450" d:DesignWidth="800">
    <Grid>
        <Grid.Resources>
            <!--圆圈边框和背景样式-->
            <Style x:Key="BorderStyle" TargetType="Border">
                <Setter Property="BorderThickness" Value="1"/>
                <Setter Property="BorderBrush" Value="{Binding CircleBorderBackground, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                <Setter Property="Background" Value="{Binding CircleBackground, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                <Setter Property="Margin" Value="5"/>
                <Setter Property="Width" Value="{Binding CircleAndTextBoxWidth, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                <Setter Property="Height" Value="{Binding Width, RelativeSource={RelativeSource Self}}"/>
                <Setter Property="CornerRadius" Value="{calc:Binding 'ActualWidth / 2', RelativeSource={RelativeSource Self}}"/>
            </Style>

            <!--圆圈内文本样式-->
            <Style x:Key="InfoStyle" TargetType="TextBlock">
                <Setter Property="HorizontalAlignment" Value="Center"/>
                <Setter Property="VerticalAlignment" Value="Center"/>
                <Setter Property="Foreground" Value="{Binding CircleForeground, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                <Setter Property="FontSize" Value="{Binding InfoFontSize, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
            </Style>

            <!--圆圈部分样式-->
            <Style x:Key="CircleStyle" TargetType="Control">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate>
                            <Grid>
                                <Border Style="{DynamicResource BorderStyle}"/>
                                <TextBlock Style="{DynamicResource InfoStyle}" 
                                           Text="{Binding CircleInfo, FallbackValue=NA, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <!--CircleWithTextBox 的输入框样式-->
            <Style x:Key="CwtbTextBoxStyle" TargetType="TextBox">
                <Setter Property="Width" Value="{Binding CircleAndTextBoxWidth, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
                <Setter Property="Height" Value="{calc:Binding 'Width * 3 / 5', RelativeSource={RelativeSource Self}}"/>
                <Setter Property="VerticalContentAlignment" Value="Center"/>
                <Setter Property="FontSize" Value="{Binding InfoFontSize, RelativeSource={RelativeSource AncestorType=UserControl}}"/>
            </Style>

            <!--DoubleUpDown 控件鼠标移上和获取键盘焦点时显示上下箭头-->
            <Style x:Key="DoubleUpDownSpinnerStyle" TargetType="{x:Type xctk:DoubleUpDown}">
                <Setter Property="ShowButtonSpinner" Value="False" />
                <Style.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="ShowButtonSpinner" Value="True" />
                    </Trigger>
                    <Trigger Property="IsKeyboardFocusWithin" Value="True">
                        <Setter Property="ShowButtonSpinner" Value="True" />
                    </Trigger>
                </Style.Triggers>
            </Style>

            <!--CircleWithTextBox 的 DoubleUpDown 控件样式-->
            <Style x:Key="CwtbDoubleUpDownStyle" TargetType="{x:Type xctk:DoubleUpDown}" BasedOn="{StaticResource DoubleUpDownSpinnerStyle}">
                <Setter Property="TextAlignment" Value="Left"/>
                <Setter Property="Minimum" Value="0"/>
                <Setter Property="Maximum" Value="100"/>
                <Setter Property="Increment" Value="1"/>
            </Style>

            <!--文本输入部分样式-->
            <Style x:Key="InputStyle" TargetType="Control">
                <!--默认为 TextBox-->
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate>
                            <Grid>
                                <TextBox Style="{markupExtensions:ResourceBinding InputStyleKey, RelativeSource={RelativeSource AncestorType=UserControl}}"
                                         Text="{Binding TextBoxValue, RelativeSource={RelativeSource AncestorType=UserControl}, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>

                <Style.Triggers>
                    <!--可切换为 Xceed.Wpf.Toolkit.DoubleUpDown-->
                    <DataTrigger Value="{x:Static local:InputStyleTagEnum.DoubleUpDown}">
                        <DataTrigger.Binding>
                            <Binding Path="InputStyleTag"
                                     RelativeSource="{RelativeSource AncestorType=UserControl}">
                            </Binding>
                        </DataTrigger.Binding>
                        <Setter Property="Template">
                            <Setter.Value>
                                <ControlTemplate>
                                    <Grid>
                                        <xctk:DoubleUpDown 
                                            Style="{markupExtensions:ResourceBinding InputStyleKey, RelativeSource={RelativeSource AncestorType=UserControl}}"
                                            Value="{Binding TextBoxValue, RelativeSource={RelativeSource AncestorType=UserControl}, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                            Width="{Binding CircleAndTextBoxWidth, RelativeSource={RelativeSource AncestorType=UserControl}}" 
                                            Height="{calc:Binding 'Width * 3 / 5', RelativeSource={RelativeSource Self}}" 
                                            FontSize="{Binding InfoFontSize, RelativeSource={RelativeSource AncestorType=UserControl}}">
                                        </xctk:DoubleUpDown>
                                    </Grid>
                                </ControlTemplate>
                            </Setter.Value>
                        </Setter>
                    </DataTrigger>
                </Style.Triggers>
            </Style>

            <!--整个控件样式-->
            <Style x:Key="CircleWithTextBoxStyle" TargetType="Control">
                <Setter Property="Template">
                    <Setter.Value>
                        <!--默认控件模板为文本框在圆圈右边-->
                        <ControlTemplate>
                            <StackPanel Orientation="Horizontal">
                                <Control Style="{DynamicResource CircleStyle}"/>
                                <Control Style="{DynamicResource InputStyle}"/>
                            </StackPanel>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>

                <Style.Triggers>
                    <!--位置标记为“左”时设置控件模板为文本框在左边，其它的类似-->
                    <DataTrigger Value="{x:Static PlacementMode.Left}">
                        <DataTrigger.Binding>
                            <Binding Path="TextBoxPlacement" 
                                     RelativeSource="{RelativeSource AncestorType=UserControl}"/>
                        </DataTrigger.Binding>
                        <Setter Property="Template">
                            <Setter.Value>
                                <ControlTemplate>
                                    <StackPanel Orientation="Horizontal">
                                        <Control Style="{DynamicResource InputStyle}"/>
                                        <Control Style="{DynamicResource CircleStyle}"/>
                                    </StackPanel>
                                </ControlTemplate>
                            </Setter.Value>
                        </Setter>
                    </DataTrigger>
                    <DataTrigger Value="{x:Static PlacementMode.Top}">
                        <DataTrigger.Binding>
                            <Binding Path="TextBoxPlacement" RelativeSource="{RelativeSource AncestorType=UserControl}"/>
                        </DataTrigger.Binding>
                        <Setter Property="Template">
                            <Setter.Value>
                                <ControlTemplate>
                                    <StackPanel>
                                        <Control Style="{DynamicResource InputStyle}"/>
                                        <Control Style="{DynamicResource CircleStyle}"/>
                                    </StackPanel>
                                </ControlTemplate>
                            </Setter.Value>
                        </Setter>
                    </DataTrigger>
                    <DataTrigger Value="{x:Static PlacementMode.Bottom}">
                        <DataTrigger.Binding>
                            <Binding Path="TextBoxPlacement" RelativeSource="{RelativeSource AncestorType=UserControl}"/>
                        </DataTrigger.Binding>
                        <Setter Property="Template">
                            <Setter.Value>
                                <ControlTemplate>
                                    <StackPanel>
                                        <Control Style="{DynamicResource CircleStyle}"/>
                                        <Control Style="{DynamicResource InputStyle}"/>
                                    </StackPanel>
                                </ControlTemplate>
                            </Setter.Value>
                        </Setter>
                    </DataTrigger>
                </Style.Triggers>
            </Style>
        </Grid.Resources>

        <!--方法一：使用 DataTrigger 切换控件模板-->
        <Control Style="{DynamicResource CircleWithTextBoxStyle}"/>

        <!--方法二：数据模板选择器（未成功，此处应该不合适）-->
        <!--<Grid>
            <Grid.Resources>
                <DataTemplate x:Key="DataTemplateLeft">
                    <StackPanel Orientation="Horizontal">
                        <TextBox Style="{DynamicResource TextBoxStyle}"/>
                        <Control Style="{DynamicResource CircleStyle}"/>
                    </StackPanel>
                </DataTemplate>
                <DataTemplate x:Key="DataTemplateTop">
                    <StackPanel>
                        <TextBox Style="{DynamicResource TextBoxStyle}"/>
                        <Control Style="{DynamicResource CircleStyle}"/>
                    </StackPanel>
                </DataTemplate>
                <DataTemplate x:Key="DataTemplateRight">
                    <StackPanel Orientation="Horizontal">
                        <Control Style="{DynamicResource CircleStyle}"/>
                        <TextBox Style="{DynamicResource TextBoxStyle}"/>
                    </StackPanel>
                </DataTemplate>
                <DataTemplate x:Key="DataTemplateBottom">
                    <StackPanel>
                        <Control Style="{DynamicResource CircleStyle}"/>
                        <TextBox Style="{DynamicResource TextBoxStyle}"/>
                    </StackPanel>
                </DataTemplate>
            </Grid.Resources>

            <ContentControl>
                <ContentControl.ContentTemplateSelector>
                    <local:CircleWithTextBoxDataTemplateSelector
                    DataTemplateLeft="{StaticResource DataTemplateLeft}"
                    DataTemplateTop="{StaticResource DataTemplateTop}"
                    DataTemplateRight="{StaticResource DataTemplateRight}"
                    DataTemplateBottom="{StaticResource DataTemplateBottom}">
                    </local:CircleWithTextBoxDataTemplateSelector>
                </ContentControl.ContentTemplateSelector>
            </ContentControl>
        </Grid>-->
    </Grid>
</UserControl>
