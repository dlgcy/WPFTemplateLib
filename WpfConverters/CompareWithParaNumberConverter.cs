using System;
using System.Globalization;
using WPFTemplateLib.Enums;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 与参数（数字）进行比较（比较操作的类型属性为 CompareAction，默认为大于）
	/// </summary>
	public class CompareWithParaNumberConverter : ValueConverterBase
	{
		/// <summary>
		/// 比较操作
		/// </summary>
		public CompareActionType CompareAction { get; set; } = CompareActionType.GreaterThan;

		/// <inheritdoc />
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			decimal.TryParse(value + "", out decimal bindValue);
			decimal.TryParse(parameter + "", out decimal paraValue);

			bool result = false;
			switch(CompareAction)
			{
				default:
				case CompareActionType.GreaterThan:
					result = bindValue > paraValue;
					break;
				case CompareActionType.GreaterThanOrEqual:
					result = bindValue >= paraValue;
					break;
				case CompareActionType.Equal:
					result = bindValue == paraValue;
					break;
				case CompareActionType.LessThanOrEqual:
					result = bindValue <= paraValue;
					break;
				case CompareActionType.LessThan:
					result = bindValue >= paraValue;
					break;
			}

			return result;
		}

		/// <inheritdoc />
		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
