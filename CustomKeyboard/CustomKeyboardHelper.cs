using System;
using System.Diagnostics;

namespace WPFTemplateLib.CustomKeyboard
{
	/// <summary>
	/// 自定义键盘弹窗帮助类
	/// </summary>
	public class CustomKeyboardHelper
	{
		/// <summary>
		/// 显示自定义键盘弹窗
		/// </summary>
		/// <param name="keyboardWindowType">键盘窗口类型</param>
		/// <param name="originValue">原始值</param>
		/// <param name="dialogSucceedAction">弹窗关闭(结果为true时)后执行的操作，参数为新值</param>
		public static void ShowCustomKeyboard(Type keyboardWindowType, string originValue, Action<string> dialogSucceedAction)
		{
			KeyboardWindowBase win = null;
			try
			{
				win = Activator.CreateInstance(keyboardWindowType, originValue) as KeyboardWindowBase;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
				Debug.WriteLine(ex);
			}

			if(win == null)
			{
				win = new NumberKeyboard(originValue);
			}

			if(win.ShowDialog() == true)
			{
				dialogSucceedAction.Invoke(win.StringValue);
			}
		}
	}
}
