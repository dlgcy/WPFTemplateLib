﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

/*
 * 源码已托管：https://gitee.com/dlgcy/WPFTemplateLib
 */
namespace WPFTemplateLib.UserControls
{
    /// <summary>
    /// 边上带文本框的圆形
    /// </summary>
    public partial class CircleWithTextBox : UserControl
    {
        public CircleWithTextBox()
        {
            InitializeComponent();
        }

        #region 依赖属性

        #region TextBox 摆放位置
        /// <summary>
        /// TextBox 摆放位置
        /// </summary>
        public PlacementMode TextBoxPlacement
        {
            get => (PlacementMode)GetValue(TextBoxPlacementProperty);
            set => SetValue(TextBoxPlacementProperty, value);
        }
        public static readonly DependencyProperty TextBoxPlacementProperty =
            DependencyProperty.Register("TextBoxPlacement", typeof(PlacementMode), 
                typeof(CircleWithTextBox), new PropertyMetadata(PlacementMode.Right));
        #endregion

        #region 圆圈内信息
        /// <summary>
        /// 圆圈内信息
        /// </summary>
        public string CircleInfo
        {
            get => (string)GetValue(CircleInfoProperty);
            set => SetValue(CircleInfoProperty, value);
        }
        public static readonly DependencyProperty CircleInfoProperty =
            DependencyProperty.Register("CircleInfo", typeof(string), 
                typeof(CircleWithTextBox), new PropertyMetadata(string.Empty));
        #endregion

        #region 文本框的值
        /// <summary>
        /// 文本框的值
        /// </summary>
        public string TextBoxValue
        {
            get => (string)GetValue(TextBoxValueProperty);
            set => SetValue(TextBoxValueProperty, value);
        }
        public static readonly DependencyProperty TextBoxValueProperty =
            DependencyProperty.Register("TextBoxValue", typeof(string), 
                typeof(CircleWithTextBox), new PropertyMetadata(string.Empty));
        #endregion

        #region 圆圈背景色
        /// <summary>
        /// 圆圈背景色
        /// </summary>
        public Brush CircleBackground
        {
            get => (Brush)GetValue(CircleBackgroundProperty);
            set => SetValue(CircleBackgroundProperty, value);
        }
        public static readonly DependencyProperty CircleBackgroundProperty =
            DependencyProperty.Register("CircleBackground", typeof(Brush), typeof(CircleWithTextBox), 
                new PropertyMetadata(new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF1E90FF"))));
        #endregion

        #region 圆圈前景色
        /// <summary>
        /// 圆圈前景色
        /// </summary>
        public Brush CircleForeground
        {
            get => (Brush)GetValue(CircleForegroundProperty);
            set => SetValue(CircleForegroundProperty, value);
        }
        public static readonly DependencyProperty CircleForegroundProperty =
            DependencyProperty.Register("CircleForeground", typeof(Brush), typeof(CircleWithTextBox), 
                new PropertyMetadata(new SolidColorBrush(Colors.Black)));
        #endregion

        #region 圆圈边框颜色
        /// <summary>
        /// 圆圈边框颜色
        /// </summary>
        public Brush CircleBorderBackground
        {
            get => (Brush)GetValue(CircleBorderBackgroundProperty);
            set => SetValue(CircleBorderBackgroundProperty, value);
        }
        public static readonly DependencyProperty CircleBorderBackgroundProperty =
            DependencyProperty.Register("CircleBorderBackground", typeof(Brush), typeof(CircleWithTextBox), 
                new PropertyMetadata(new SolidColorBrush(Colors.DarkCyan)));
        #endregion

        #region 字体大小
        /// <summary>
        /// 圆圈内和文本框的字体大小（默认16）
        /// </summary>
        public double InfoFontSize
        {
            get => (double)GetValue(InfoFontSizeProperty);
            set => SetValue(InfoFontSizeProperty, value);
        }
        public static readonly DependencyProperty InfoFontSizeProperty =
            DependencyProperty.Register("InfoFontSize", typeof(double), typeof(CircleWithTextBox), new PropertyMetadata(16d));
        #endregion

        #region 圆圈和文本框的宽度
        /// <summary>
        /// 圆圈和文本框的宽度（默认50）
        /// </summary>
        public double CircleAndTextBoxWidth
        {
            get => (double)GetValue(CircleAndTextBoxWidthProperty);
            set => SetValue(CircleAndTextBoxWidthProperty, value);
        }
        public static readonly DependencyProperty CircleAndTextBoxWidthProperty =
            DependencyProperty.Register("CircleAndTextBoxWidth", typeof(double), typeof(CircleWithTextBox), new PropertyMetadata(50d));
        #endregion

        //以下两个输入部分的属性，默认使用TextBox时不用设置，使用其它的需要一起设置

        #region 输入部分的样式标识
        /// <summary>
        /// 输入部分的样式标识（默认为空，使用默认的TextBox，其它的自行定义）
        /// </summary>
        public InputStyleTagEnum InputStyleTag
        {
            get => (InputStyleTagEnum)GetValue(InputStyleTagProperty);
            set => SetValue(InputStyleTagProperty, value);
        }
        public static readonly DependencyProperty InputStyleTagProperty =
            DependencyProperty.Register("InputStyleTag", typeof(InputStyleTagEnum), typeof(CircleWithTextBox), new PropertyMetadata(InputStyleTagEnum.TextBox));
        #endregion

        #region 输入部分的样式Key
        /// <summary>
        /// 输入部分的样式 Key，用于 ResourceBinding，提供的 Key 对应的样式可设置不影响功能的属性
        /// </summary>
        public string InputStyleKey
        {
            get => (string)GetValue(InputStyleKeyProperty);
            set => SetValue(InputStyleKeyProperty, value);
        }
        public static readonly DependencyProperty InputStyleKeyProperty =
            DependencyProperty.Register("InputStyleKey", typeof(string), typeof(CircleWithTextBox), new PropertyMetadata(InputStyleKeyEnum.CwtbTextBoxStyle.ToString()));
        #endregion

        #endregion
    }

    /// <summary>
    /// 输入部分样式标识枚举
    /// </summary>
    public enum InputStyleTagEnum
    {
        TextBox,

        /// <summary>
        /// Xceed.Wpf.Toolkit.DoubleUpDown
        /// </summary>
        DoubleUpDown,
    }

    /// <summary>
    /// 输入部分样式Key枚举
    /// </summary>
    public enum InputStyleKeyEnum
    {
        CwtbTextBoxStyle,
        CwtbDoubleUpDownStyle,
    }

    /// <summary>
    /// CircleWithTextBox 的数据模板选择器
    /// </summary>
    public class CircleWithTextBoxDataTemplateSelector : DataTemplateSelector
    {
        /// <summary>
        /// 文本框在左
        /// </summary>
        public DataTemplate DataTemplateLeft { get; set; }

        /// <summary>
        /// 文本框在上
        /// </summary>
        public DataTemplate DataTemplateTop { get; set; }

        /// <summary>
        /// 文本框在右
        /// </summary>
        public DataTemplate DataTemplateRight { get; set; }

        /// <summary>
        /// 文本框在下
        /// </summary>
        public DataTemplate DataTemplateBottom { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            try
            {
                Type type = item?.GetType();
                if (type != null)
                {
                    PropertyInfo propertyInfo = type.GetProperty("TextBoxPlacement");
                    if (propertyInfo == null)
                    {
                        return DataTemplateRight;
                    }

                    PlacementMode placementMode = (PlacementMode)propertyInfo.GetValue(item, null);
                    switch (placementMode)
                    {
                        case PlacementMode.Left:
                            return DataTemplateLeft;
                        case PlacementMode.Top:
                            return DataTemplateTop;
                        case PlacementMode.Right:
                            return DataTemplateRight;
                        case PlacementMode.Bottom:
                            return DataTemplateBottom;
                        default:
                            return DataTemplateRight;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"模板选择器发生异常：{ex}");
            }

            return base.SelectTemplate(item, container);
        }
    }
}
