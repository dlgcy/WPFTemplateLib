using System.Windows;
using System.Windows.Controls;

namespace WPFTemplateLib.WpfHelpers
{
	/// <summary>
	/// FrameworkElement 帮助类
	/// </summary>
	public static class FrameworkElementHelper
	{
		/// <summary>
		/// 获取渲染变换的原点（在 Canvas 中）
		/// </summary>
		/// <param name="element">元素</param>
		/// <param name="originLeftTopGap">原点与左上角的坐标相减的值（如果不需要可使用 <code>out _</code> 忽略）</param>
		/// <param name="isFlipOrigin">是否翻转原点</param>
		/// <returns>原点（Canvas 中的 Left,Top 坐标）</returns>
		public static Point GetRenderTransformOriginPointInCanvas(this FrameworkElement element, out Point originLeftTopGap, bool isFlipOrigin = false)
		{
			Point renderTransformOrigin = element.RenderTransformOrigin;
			if(isFlipOrigin)
			{
				renderTransformOrigin = new Point(1 - renderTransformOrigin.X, 1 - renderTransformOrigin.Y);
			}

			double width = element.Width;
			double height = element.Height;
			double left = Canvas.GetLeft(element);
			double top = Canvas.GetTop(element);
			double transformX = left + width * renderTransformOrigin.X;
			double transformY = top + height * renderTransformOrigin.Y;

			double gapX = transformX - left;
			double gapY = transformY - top;
			originLeftTopGap = new Point(gapX, gapY);

			Point transformPoint = new Point(transformX, transformY);
			return transformPoint;
		}
	}
}
