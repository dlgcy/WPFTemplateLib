using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using WPFTemplateLib.Mvvm;

namespace WPFTemplateLib.Models
{
	/// <summary>
	/// 基础树节点类
	/// </summary>
	public class BaseTreeNode : SimpleBindableBase
	{
		#region 属性

		/// <summary>
		/// 父节点
		/// </summary>
		public BaseTreeNode Parent { get; set; }

		/// <summary>
		/// 子节点集合（添加子节点时会自动设置父节点）
		/// </summary>
		public ObservableCollection<BaseTreeNode> Children { get; set; } = new();

		/// <summary>
		/// 是否有子节点
		/// </summary>
		public bool HasChildren => Children.Count > 0;

		/// <summary>
		/// 层级
		/// </summary>
		public int Level { get; set; }

		#endregion

		#region 方法

		/// <inheritdoc />
		protected override void PropertyChangedHandle(object sender, PropertyChangedEventArgs e)
		{
			base.PropertyChangedHandle(sender, e);
			switch(e.PropertyName)
			{
				case nameof(Children):
				{
					if(Children != null)
					{
						foreach(BaseTreeNode child in Children)
						{
							child.Parent = this;
						}

						InitChildrenEvent();
					}
					break;
				}
				case nameof(Level):
				{
					RefreshChildrenLevel();
					break;
				}
				case nameof(Parent):
				{
					if(Parent != null)
					{
						Level = Parent.Level + 1;
					}
					break;
				}
			}
		}

		/// <summary>
		/// 初始化子节点集合事件
		/// </summary>
		private void InitChildrenEvent()
		{
			Children.CollectionChanged -= Children_CollectionChanged;
			Children.CollectionChanged += Children_CollectionChanged;
		}

		/// <summary>
		/// 子节点集合变更事件
		/// </summary>
		private void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if(e.NewItems != null)
			{
				foreach(var newItem in e.NewItems)
				{
					var item = newItem as BaseTreeNode;
					if(item == null)
						continue;

					item.Parent = this;
				}
			}

			if(e.OldItems != null)
			{
				foreach(var oldItem in e.OldItems)
				{
					var item = oldItem as BaseTreeNode;
					if(item == null)
						continue;

					item.Parent = null;
				}
			}

			OnPropertyChanged(nameof(HasChildren));
		}

		/// <summary>
		/// 刷新子节点 Level
		/// </summary>
		public void RefreshChildrenLevel()
		{
			foreach(var child in Children)
			{
				child.Level = Level + 1;
				child.RefreshChildrenLevel();
			}
		}

		/// <summary>
		/// 获取所有叶子节点
		/// </summary>
		/// <param name="levelLimitHigherOrEqual">层级限制（大等于指定层级才算叶子节点）</param>
		/// <param name="isOnlyChecked">是否只选取勾选的</param>
		/// <returns></returns>
		public virtual List<BaseTreeNode> GetLeafs(int? levelLimitHigherOrEqual = null)
		{
			var list = new List<BaseTreeNode>();
			foreach(var child in Children)
			{
				if(!child.HasChildren)
				{
					if(levelLimitHigherOrEqual == null || child.Level >= levelLimitHigherOrEqual)
					{
						list.Add(child);
					}
				}
				else
				{
					var childrenLeafs = child.GetLeafs(levelLimitHigherOrEqual);
					list.AddRange(childrenLeafs);
				}
			}

			return list;
		}

		#endregion
	}
}
