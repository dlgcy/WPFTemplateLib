using System;
using System.Diagnostics;
using System.Globalization;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 参考值转实际值
	/// <example>
	///	<code>
	///	Canvas.Top="{Binding Position, 
	///	Converter={converters:ReferenceValueToRealValueConverter IsReversed=True, RealValueMin=430, RealValueMax=460, RefValueMin=0, RefValueMax=100}}"
	/// </code>
	/// </example>
	/// </summary>
	public class ReferenceValueToRealValueConverter : ValueConverterBase
	{
		public double RefValueMin { get; set; }
		public double RefValueMax { get; set; }
		public double RealValueMin { get; set; }
		public double RealValueMax { get; set; }

		/// <summary>
		/// 是否调转方向
		/// </summary>
		public bool IsReversed { get; set; }

		/// <inheritdoc />
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double realValue = RealValueMin;
			try
			{
				if(!double.TryParse(value + "", out double originValue))
				{
					throw new ArgumentException($"绑定值[{value + ""}]无法转为 double");
				}

				if(originValue < RefValueMin) originValue = RefValueMin;
				if(originValue > RefValueMax) originValue = RefValueMax;

				double ratio = (originValue - RefValueMin) / (RefValueMax - RefValueMin);
				double gap = ratio * (RealValueMax - RealValueMin);
				if(IsReversed)
				{
					realValue = RealValueMax - gap;
				}
				else
				{
					realValue = gap + RealValueMin;
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
				Debug.WriteLine($"[{nameof(ReferenceValueToRealValueConverter)}] Exception：{ex}");
			}
			return realValue;
		}

		/// <inheritdoc />
		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
