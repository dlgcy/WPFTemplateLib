using System;
using System.Globalization;
using System.Windows.Data;
using DotNet.Utilities;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// 获取枚举描述的转换器
    /// </summary>
    public class GetEnumDescriptionConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = value as Enum;
            string description = myEnum?.GetEnumDescription() ?? "";
            return description;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
