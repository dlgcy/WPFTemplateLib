using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] DataGridRow 转行号（适用于无删除行功能的情况）
	/// https://www.cnblogs.com/y-yp/p/7691248.html
	/// </summary>
	/// <example>
	///	<code>
	///	&lt;DataGridTextColumn Header="序号"
	///		Binding="{Binding RelativeSource={RelativeSource AncestorType=DataGridRow}, Converter={StaticResource DataGridRowToNoConverter}}"/>
	/// </code>
	/// </example>
	public class DataGridRowToNoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
			if (value is DataGridRow row)
                return row.GetIndex() + 1;
            else
                return -1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
