using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [多值转换器] 判断列表是否包含元素并返回指定值
	/// </summary>
	/// <remarks>
	/// <para/>绑定规则：第一个为列表，后续为需要判断是否包含的元素(1~n个)，包含任意一个则判定为包含，返回true返回值。<para/>
	/// 参数规则：【true返回值:false返回值】<para/>
	///	绑定内容数小于2，或者绑定的第一个对象不是<see cref="IList"/>类型，则返回false返回值。
	/// </remarks>
	public class ListContainsElementMultiConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			string paraStr = parameter + "";
			//将参数字符串分段 parray[0]为true返回值，parray[1]为false返回值;
			string[] paraArray = paraStr.Split(':');

			if(string.IsNullOrWhiteSpace(paraStr) || paraArray.Length < 2)
			{
				Console.WriteLine($"参数[{paraStr}]非法，需包含':'符且左右各有合适的内容。已替换为\"true:false\"。");
				paraStr = "true:false";
				paraArray = paraStr.Split(':');
			}

			bool isContains = false;
			if(values.Length >= 2 && values[0] is IList list)
			{
				isContains = values.Skip(1).ToList().Exists(x=> list.Contains(x));
			}

			return isContains ? paraArray[0] : paraArray[1];
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
