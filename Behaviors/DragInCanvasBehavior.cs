﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Xaml.Behaviors;

namespace WPFTemplateLib.Behaviors
{
    /// <summary>
    /// 在Canvas中拖动的行为
    /// https://www.cnblogs.com/ListenFly/p/3275289.html
    /// </summary>
    /// <example>
    /// <code>
    /// &lt;b:Interaction.Behaviors&gt;
    ///     &lt;behaviors:DragInCanvasBehavior/&gt;
    /// &lt;/b:Interaction.Behaviors&gt;
    /// </code>
    /// </example>
    public class DragInCanvasBehavior : Behavior<UIElement>
    {
        //元素父节点
        private Canvas canvas;
        //标识是否进入拖动
        private bool isDraging = false;
        //按下鼠标时的坐标(用于计算要移动的位置)
        private Point mouseOffset;

        /// <summary>
        /// 附加行为后
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            //添加鼠标事件(AssociatedObject也就是当前应用此Behavior的元素)
            AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove += AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
        }

        /// <summary>
        /// 鼠标左键抬起
        /// </summary>
        void AssociatedObject_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //释放拖动状态
            isDraging = false;
            AssociatedObject.ReleaseMouseCapture();
        }

        /// <summary>
        /// 鼠标移动
        /// </summary>
        void AssociatedObject_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //如果进入拖动状态
            if (isDraging)
            {
                //得到新的位置
                Point newPoint = e.GetPosition(canvas);
                //旧的坐标加上新坐标和旧坐标的差
                mouseOffset.X += newPoint.X - mouseOffset.X;
                mouseOffset.Y += newPoint.Y - mouseOffset.Y;

                //设置元素的Left和Top，之所以要用X(Y)减去Width(Height)，主要是为了使鼠标在元素中心
                Canvas.SetLeft(AssociatedObject, mouseOffset.X - (AssociatedObject as FrameworkElement).ActualWidth / 2);
                Canvas.SetTop(AssociatedObject, mouseOffset.Y - (AssociatedObject as FrameworkElement).ActualHeight / 2);
            }
        }

        /// <summary>
        /// 鼠标左键按下
        /// </summary>
        void AssociatedObject_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //将元素的父节点元素赋值为Canvas(之所以使用Canvas，是因为Canvas容易动态布局)
            if (canvas == null)
                canvas = (Canvas)VisualTreeHelper.GetParent(AssociatedObject);
            //进入拖动状态
            isDraging = true;
            //获得初始位置
            mouseOffset = e.GetPosition(AssociatedObject);
            AssociatedObject.CaptureMouse();
        }

        /// <summary>
        /// 分离行为
        /// </summary>
        protected override void OnDetaching()
        {
            //移除鼠标事件
            AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            AssociatedObject.MouseMove -= AssociatedObject_MouseMove;
            AssociatedObject.MouseLeftButtonUp -= AssociatedObject_MouseLeftButtonUp;
        }
    }
}
