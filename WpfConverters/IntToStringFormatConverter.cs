using System;
using System.Globalization;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// 【未成功】[转换器] int 转 StringFormat 字符串（绑定的 int 值代表位数）
	/// </summary>
	public class IntToStringFormatConverter : ValueConverterBase
	{
		/// <summary>
		/// 格式化类型字符串（如：[默认] F-小数，C-货币，D-整数，N-逗号分隔的小数，P-百分比）
		/// </summary>
		public string FormatTypeStr { get; set; } = "F";

		/// <summary>
		/// 前缀
		/// </summary>
		public string Prefix { get; set; } = string.Empty;

		/// <summary>
		/// 尾缀
		/// </summary>
		public string Suffix { get; set; } = string.Empty;

		/// <inheritdoc />
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int.TryParse(value + "", out int num);
				if(num < 0)
				{
					Console.WriteLine($"位数[{num}]小于0无意义，已替换为0");
					num = 0;
				}

				string result = Prefix + "{}{0:" + FormatTypeStr + num + "}" + Suffix;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				return string.Empty;
			}
		}

		/// <inheritdoc />
		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
