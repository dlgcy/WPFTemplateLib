using System;
using System.Globalization;
using DotNet.Utilities;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 显示枚举信息
	/// </summary>
	public class ShowEnumInfoConverter : ValueConverterBase
	{
		public string ValueTag { get; set; } = "Value";
		public string KeyTag { get; set; } = "Key";
		public string DescTag { get; set; } = "Desc";

		/// <summary>
		/// 格式化字符串（默认为"Value、Key（Desc）"）
		/// </summary>
		public string Format { get; set; }

		public ShowEnumInfoConverter()
		{
			Format = $"{ValueTag}、{KeyTag}（{DescTag}）";
		}

		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Enum theEnum = value as Enum;
			if(theEnum == null)
				return string.Empty;

			string description = theEnum.GetEnumDescription() ?? "";
			string key = theEnum.ToString();

			Type enumType = theEnum.GetType();
			Type underlyingType = Enum.GetUnderlyingType(enumType); //获取枚举的底层类型
			object underlyingValue = System.Convert.ChangeType(theEnum, underlyingType);
			string valueStr = underlyingValue.ToString();

			string result;
			if(string.IsNullOrWhiteSpace(Format))
			{
				result = $"{valueStr}、{key}（{description}）";
			}
			else
			{
				result = Format.Replace(ValueTag, valueStr)
					.Replace(KeyTag, key)
					.Replace(DescTag, description);
			}

			return result;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
