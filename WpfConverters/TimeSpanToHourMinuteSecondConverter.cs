using System;
using System.Globalization;
using System.Windows.Data;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] TimeSpan 转 HH:mm:ss 格式
	/// </summary>
	public class TimeSpanToHourMinuteSecondConverter : ValueConverterBase
	{
		/// <inheritdoc />
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string result = "00:00:00";
			if(value is not TimeSpan timeSpan)
			{
				return result;
			}

			result = Math.Truncate(timeSpan.TotalHours).ToString("#00") + ":" + timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00");
			return result;
		}

		/// <inheritdoc />
		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}
