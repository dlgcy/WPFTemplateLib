using WPFTemplateLib.Mvvm;

namespace WPFTemplateLib.Models
{
	public class PvSvData<T> : SimpleBindableBase
	{
		public PvSvData()
		{
		}

		public PvSvData(string tag)
		{
			Tag = tag;
			Name = tag;
		}

		public int No { get; set; }

		public string Name { get; set; }

		public string Tag { get; set; }

		/// <summary>
		/// 当前值
		/// </summary>
		public T Pv { get; set; }

		/// <summary>
		/// 设定值（当前）
		/// </summary>
		public T SvCurrent { get; set; }

		/// <summary>
		/// 设定值（设置）
		/// </summary>
		public T SvSet { get; set; }

		/// <summary>
		/// 最小值
		/// </summary>
		public T MinValue { get; set; }

		/// <summary>
		/// 最大值
		/// </summary>
		public T MaxValue { get; set; }
	}
}
