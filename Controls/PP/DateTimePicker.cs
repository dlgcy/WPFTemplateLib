using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Calendar = System.Windows.Controls.Calendar;

namespace WPFTemplateLib.Controls.PP
{
	/// <summary>
	/// 时间日期选择器
	/// 来自：https://gitee.com/DLGCY_Clone/PP.Wpf
	/// </summary>
	[TemplatePart(Name = "PART_TextBox", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_IconButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_Popup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_ConfirmButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_NowButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_ClearButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_Calendar", Type = typeof(Calendar))]
    public class DateTimePicker : Control
    {
        #region DependencyProperties

        /// <summary>
        /// 选择的时间
        /// </summary>
        public static readonly DependencyProperty SelectedDateTimeProperty = DependencyProperty.Register(nameof(SelectedDateTime), typeof(DateTime?), typeof(DateTimePicker), 
			new FrameworkPropertyMetadata(DateTime.Now, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSelectedDatePropertyChanged));
        private static void OnSelectedDatePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePicker)d).OnSelectedDateChanged((DateTime?)e.NewValue);
        }
        /// <summary>
        /// 选择的时间
        /// </summary>
        public DateTime? SelectedDateTime { get => (DateTime?)GetValue(SelectedDateTimeProperty); set => SetValue(SelectedDateTimeProperty, value); }

        /// <summary>
        /// 显示文本
        /// </summary>
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(Text), typeof(string), typeof(DateTimePicker), new PropertyMetadata(OnTextPropertyChanged));
        private static void OnTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var picker = (DateTimePicker)d;
            var txt = (string)e.NewValue;

            if (DateTime.TryParseExact(txt, picker.DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime time))
                picker.SelectedDateTime = time;
        }
        /// <summary>
        /// 显示文本
        /// </summary>
        public string Text { get => (string)GetValue(TextProperty); set => SetValue(TextProperty, value); }

        /// <summary>
        /// 日期时间显示格式
        /// </summary>
        public static readonly DependencyProperty DateTimeFormatProperty = DependencyProperty.Register(nameof(DateTimeFormat), typeof(string), typeof(DateTimePicker), new PropertyMetadata("yyyy-MM-dd HH:mm:ss"));
        /// <summary>
        /// 日期时间显示格式
        /// </summary>
        public string DateTimeFormat { get => (string)GetValue(DateTimeFormatProperty); set => SetValue(DateTimeFormatProperty, value); }

        /// <summary>
        /// 小时列表
        /// </summary>
        public static readonly DependencyPropertyKey HoursPropertyKey = DependencyProperty.RegisterReadOnly("Hours", typeof(IEnumerable<int>), typeof(DateTimePicker), new PropertyMetadata(default));
        /// <summary>
        /// 小时列表
        /// </summary>
        public static readonly DependencyProperty HoursProperty = HoursPropertyKey.DependencyProperty;
        /// <summary>
        /// 小时列表
        /// </summary>
        public IEnumerable<int> Hours { get => (IEnumerable<int>)GetValue(HoursProperty); private set => SetValue(HoursPropertyKey, value); }

        /// <summary>
        /// 小时
        /// </summary>
        public static readonly DependencyProperty HourProperty = DependencyProperty.Register(nameof(Hour), typeof(int), typeof(DateTimePicker), new PropertyMetadata(OnHourPropertyChagned));
        private static void OnHourPropertyChagned(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePicker)d).BeginUpdatePreText();
        }
        /// <summary>
        /// 小时
        /// </summary>
        public int Hour { get => (int)GetValue(HourProperty); set => SetValue(HourProperty, value); }

        /// <summary>
        /// 分钟列表
        /// </summary>
        public static readonly DependencyPropertyKey MinutesPropertyKey = DependencyProperty.RegisterReadOnly("Minutes", typeof(IEnumerable<int>), typeof(DateTimePicker), new PropertyMetadata(default));
        /// <summary>
        /// 分钟列表
        /// </summary>
        public static readonly DependencyProperty MinutesProperty = MinutesPropertyKey.DependencyProperty;
        /// <summary>
        /// 分钟列表
        /// </summary>
        public IEnumerable<int> Minutes { get => (IEnumerable<int>)GetValue(MinutesProperty); private set => SetValue(MinutesPropertyKey, value); }

        /// <summary>
        /// 分钟
        /// </summary>
        public static readonly DependencyProperty MinuteProperty = DependencyProperty.Register(nameof(Minute), typeof(int), typeof(DateTimePicker), new PropertyMetadata(OnMinutePropertyChagned));
        private static void OnMinutePropertyChagned(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePicker)d).BeginUpdatePreText();
        }
        /// <summary>
        /// 分钟
        /// </summary>
        public int Minute { get => (int)GetValue(MinuteProperty); set => SetValue(MinuteProperty, value); }

        /// <summary>
        /// 秒列表
        /// </summary>
        public static readonly DependencyPropertyKey SecondsPropertyKey = DependencyProperty.RegisterReadOnly("Seconds", typeof(IEnumerable<int>), typeof(DateTimePicker), new PropertyMetadata(default));
        /// <summary>
        /// 秒列表
        /// </summary>
        public static readonly DependencyProperty SecondsProperty = SecondsPropertyKey.DependencyProperty;
        /// <summary>
        /// 秒列表
        /// </summary>
        public IEnumerable<int> Seconds { get => (IEnumerable<int>)GetValue(SecondsProperty); private set => SetValue(SecondsPropertyKey, value); }

        /// <summary>
        /// 秒
        /// </summary>
        public static readonly DependencyProperty SecondProperty = DependencyProperty.Register(nameof(Second), typeof(int), typeof(DateTimePicker), new PropertyMetadata(OnSecondPropertyChagned));
        private static void OnSecondPropertyChagned(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePicker)d).BeginUpdatePreText();
        }
        /// <summary>
        /// 秒
        /// </summary>
        public int Second { get => (int)GetValue(SecondProperty); set => SetValue(SecondProperty, value); }

        /// <summary>
        /// 日期预览
        /// </summary>
        public static readonly DependencyPropertyKey PrevTextPropertyKey = DependencyProperty.RegisterReadOnly("PrevText", typeof(string), typeof(DateTimePicker), new PropertyMetadata(default));
        /// <summary>
        /// 日期预览
        /// </summary>
        public static readonly DependencyProperty PrevTextProperty = PrevTextPropertyKey.DependencyProperty;
        /// <summary>
        /// 日期预览
        /// </summary>
        public string PrevText { get => (string)GetValue(PrevTextProperty); private set => SetValue(PrevTextPropertyKey, value); }

        public static readonly DependencyProperty IsReadOnlyProperty = TextBoxBase.IsReadOnlyProperty.AddOwner(typeof(DateTimePicker));
        /// <summary>
        /// 只读
        /// </summary>
        public bool IsReadOnly { get => (bool)GetValue(IsReadOnlyProperty); set => SetValue(IsReadOnlyProperty, value); }

        #endregion

        static DateTimePicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DateTimePicker), new FrameworkPropertyMetadata(typeof(DateTimePicker)));
        }

        /// <summary>
        /// 时间日期选择器
        /// </summary>
        public DateTimePicker()
        {
            Hours = Enumerable.Range(0, 24);
            Minutes = Seconds = Enumerable.Range(0, 60);
        }

        #region Public Methods

        public void SelectAll()
        {
            _input?.SelectAll();
        }

        #endregion

        #region Override Methods

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (_input != null && _popup != null && _calendar != null)
            {
                _input.PreviewMouseLeftButtonUp -= OnInputPreviewMouseLeftButtonUp;
                _input.LostFocus -= OnInputLostFocus;

                _calendar.SelectedDatesChanged -= OnCalendarSelectedDatesChanged;

                if (_btnIcon != null)
                    _btnIcon.Click -= OnPrevOpenPopup;

                if (_btnConfirm != null)
                    _btnConfirm.Click -= OnConfirmButtonClick;

                if (_btnNow != null)
                    _btnNow.Click -= OnNowButtonClick;

                if (_btnClear != null)
                    _btnClear.Click -= OnClearButtonClick;

                var lists = new List<ListBox> { _list1, _list2, _list3 };

                foreach (var list in lists)
                {
                    if (list == null)
                        continue;

                    list.SelectionChanged -= OnListBoxSelectionChanged;
                    list.Loaded -= OnListBoxLoaded;
                }
            }

            _input = Template.FindName("PART_TextBox", this) as TextBox;
            _popup = Template.FindName("PART_Popup", this) as Popup;
            _calendar = _popup?.FindName("PART_Calendar") as Calendar;

            if (_input != null && _popup != null && _calendar != null)
            {
                OnSelectedDateChanged(SelectedDateTime);

                _input.PreviewMouseLeftButtonUp += OnInputPreviewMouseLeftButtonUp;
                _input.LostFocus += OnInputLostFocus;

                _calendar.SelectionMode = CalendarSelectionMode.SingleDate;
                _calendar.SelectedDatesChanged += OnCalendarSelectedDatesChanged;

                _btnIcon = Template.FindName("PART_IconButton", this) as Button;
                _btnConfirm = Template.FindName("PART_ConfirmButton", this) as Button;
                _btnNow = Template.FindName("PART_NowButton", this) as Button;
                _btnClear = Template.FindName("PART_ClearButton", this) as Button;

                if (_btnIcon != null)
                    _btnIcon.Click += OnPrevOpenPopup;

                if (_btnConfirm != null)
                    _btnConfirm.Click += OnConfirmButtonClick;

                if (_btnNow != null)
                    _btnNow.Click += OnNowButtonClick;

                if (_btnClear != null)
                    _btnClear.Click += OnClearButtonClick;

                _list1 = _popup.FindName("list1") as ListBox;
                _list2 = _popup.FindName("list2") as ListBox;
                _list3 = _popup.FindName("list3") as ListBox;

                var lists = new List<ListBox> { _list1, _list2, _list3 };

                foreach (var list in lists)
                {
                    if (list == null)
                        continue;

                    list.SelectionMode = SelectionMode.Single;
                    list.SelectionChanged += OnListBoxSelectionChanged;
                    list.Loaded += OnListBoxLoaded;
                }
            }
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (_input != null && _input.Focus())
                e.Handled = true;
            else
                base.OnGotFocus(e);
        }

        #endregion

        #region Private Methods

        private void OnSelectedDateChanged(DateTime? date)
        {
            var today = DateTime.Now;

            DateTime time;

            if (date.HasValue)
            {
                time = date.Value;
                Text = time.ToString(DateTimeFormat, CultureInfo.InvariantCulture);
            }
            else
            {
                Text = null;
                time = today;
            }

            _year = time.Year;
            _month = time.Month;
            _day = time.Day;
            Hour = time.Hour;
            Minute = time.Minute;
            Second = time.Second;

            if (_calendar != null)
                _calendar.SelectedDate = time;
        }

        private void OnPrevOpenPopup(object sender, RoutedEventArgs e)
        {
            _popup.IsOpen = true;
        }

        private void OnClearButtonClick(object sender, RoutedEventArgs e)
        {
            if (SelectedDateTime == null)
                OnSelectedDateChanged(null);
            else
                SelectedDateTime = null;
        }

        private void OnNowButtonClick(object sender, RoutedEventArgs e)
        {
            SelectedDateTime = DateTime.Now;
        }


        private void OnInputPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Dispatcher.InvokeAsync(() =>
            {
                if (_input.IsFocused)
                    OnPrevOpenPopup(sender, e);
            });
        }

        private void OnInputLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Text) || !DateTime.TryParse(Text, out DateTime date))
            {
                Text = null;
                SelectedDateTime = null;
            }
            else
                SelectedDateTime = date;
        }

        private void OnConfirmButtonClick(object sender, RoutedEventArgs e)
        {
            SelectedDateTime = _prevDate;

            _popup.IsOpen = false;

            _input.Focus();
            _input.SelectionLength = 0;
            _input.SelectionStart = _input.Text == null ? 0 : _input.Text.Length;
        }

        private void OnCalendarSelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            _calendar.ReleaseStylusCapture();

            var time = _calendar.SelectedDate ?? DateTime.Now;

            _year = time.Year;
            _month = time.Month;
            _day = time.Day;

            BeginUpdatePreText();
        }

        private void OnListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ScrollToCenter((ListBox)sender);
        }

        private void OnListBoxLoaded(object sender, RoutedEventArgs e)
        {
            ScrollToCenter((ListBox)sender);
        }

        private void ScrollToCenter(ListBox list)
        {
            if (!list.IsLoaded || list.SelectedItem == null || (list.IsMouseOver && Mouse.LeftButton == MouseButtonState.Pressed))
                return;

            var item = (ListBoxItem)list.ItemContainerGenerator.ContainerFromIndex(list.SelectedIndex);

            if (item == null)
                return;

            var sv = list.Template.FindName("PART_ScrollViewer", list) as ScrollViewer;

            if (sv == null)
                return;

            var offsetY = item.TranslatePoint(new Point(0, -(sv.ViewportHeight - item.ActualHeight) / 2), sv).Y;

            sv.ScrollToVerticalOffset(sv.VerticalOffset + offsetY);
        }

        private void BeginUpdatePreText()
        {
            _action = UpdatePreText;

            Dispatcher.InvokeAsync(() =>
            {
                if (_action != null)
                {
                    _action.Invoke();
                    _action = null;
                }
            });
        }

        private void UpdatePreText()
        {
            _prevDate = new DateTime(_year, _month, _day, Hour, Minute, Second);
            PrevText = _prevDate.ToString(DateTimeFormat);
        }

        #endregion

        #region Fields

        private TextBox _input;
        private Popup _popup;
        private Button _btnIcon, _btnConfirm, _btnNow, _btnClear;
        private Calendar _calendar;
        private Action _action;
        private ListBox _list1, _list2, _list3;
        private DateTime _prevDate;
        private int _year, _month, _day;

        #endregion
    }
}
