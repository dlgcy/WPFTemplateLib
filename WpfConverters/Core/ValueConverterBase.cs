using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace WPFTemplateLib.WpfConverters.Core
{
	/// <summary>
	/// 值转换器基类
	/// 参见：https://www.bilibili.com/video/BV18M41147jR/
	/// </summary>
	public abstract class ValueConverterBase : MarkupExtension, IValueConverter
	{
		/// <inheritdoc />
		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}

		/// <inheritdoc />
		public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

		/// <inheritdoc />
		public abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);
	}
}
