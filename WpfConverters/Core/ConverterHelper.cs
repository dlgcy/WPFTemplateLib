using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using WPFTemplateLib.WpfHelpers;

namespace WPFTemplateLib.WpfConverters.Core
{
	public static class ConverterHelper
	{
		#region 转换值（迁移自 MultiObjectConverter）

		/// <summary>
		/// 转换值
		/// </summary>
		/// <param name="valueStr">值的字符串形式</param>
		/// <param name="convertType">转换类型</param>
		/// <returns>转换后的值</returns>
		public static object ConvertValue(string valueStr, ConvertResultType convertType)
		{
			try
			{
				switch(convertType)
				{
					default:
					case ConvertResultType.String:
						return valueStr;

					case ConvertResultType.Visibility:
					{
						if(valueStr.Equals("Collapsed", StringComparison.OrdinalIgnoreCase))
						{
							return Visibility.Collapsed;
						}
						else if(valueStr.Equals("Hidden", StringComparison.OrdinalIgnoreCase))
						{
							return Visibility.Hidden;
						}
						else
						{
							return Visibility.Visible;
						}
					}

					case ConvertResultType.Bool:
						return Convert.ToBoolean(valueStr);

					case ConvertResultType.Int:
						return int.Parse(valueStr);
					case ConvertResultType.Float:
						return float.Parse(valueStr);
					case ConvertResultType.Double:
						return double.Parse(valueStr);
					case ConvertResultType.SolidColorBrush:
						return new SolidColorBrush(MediaColorHelper.ColorStrToMediaColor(valueStr));
					case ConvertResultType.UInt:
						return uint.Parse(valueStr);
					case ConvertResultType.Short:
						return short.Parse(valueStr);
					case ConvertResultType.UShort:
						return ushort.Parse(valueStr);
					case ConvertResultType.Byte:
						return byte.Parse(valueStr);
					case ConvertResultType.Long:
						return long.Parse(valueStr);
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine($"[{nameof(ConverterHelper)}] 转换值异常：{ex}");
				return valueStr;
			}
		}

		/// <summary>
		/// 转换值
		/// </summary>
		/// <param name="valueStr">值的字符串形式</param>
		/// <param name="typeEnumStr">转换类型枚举的字符串形式(name 或 value)</param>
		/// <returns>转换后的值</returns>
		public static object ConvertValue(string valueStr, string typeEnumStr)
		{
			ConvertResultType convertType = (ConvertResultType)Enum.Parse(typeof(ConvertResultType), typeEnumStr);
			return ConvertValue(valueStr, convertType);
		}

		#endregion

		/// <summary>
		/// 转换值数组为 bool 列表
		/// </summary>
		/// <remarks>
		///	bool 类型直接判断；string 类型判断IsNullOrWhiteSpace；其它类型使用 double.TryParse 转换，判断是否大于零。
		/// </remarks>
		/// <returns>bool 列表</returns>
		public static List<bool> ConvertValuesToBoolList(object[] values)
		{
			List<bool> boolList = new List<bool>();
			foreach(object obj in values)
			{
				bool boolItem = false;

				if(obj is bool b)
				{
					boolItem = b;
				}
				else if(obj is string s)
				{
					boolItem = !string.IsNullOrWhiteSpace(s);
				}
				else if(double.TryParse(obj + "", out double d))
				{
					boolItem = d > 0;
				}

				boolList.Add(boolItem);
			}

			return boolList;
		}
	}
}
