using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 浮点数转科学计数法
	/// </summary>
	public class FloatToScienceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool isNum = float.TryParse(value + "", out float fValue);
			if (!isNum)
			{
				return "";
			}

			string sciene = fValue.ToString("0.0E+0");
			return sciene;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
