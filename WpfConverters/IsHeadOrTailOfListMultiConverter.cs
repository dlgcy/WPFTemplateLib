using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;
using WPFTemplateLib.WpfConverters.Core;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [多值转换器] 判断元素对象是否在列表的头或尾
	/// </summary>
	/// <remarks>
	/// <para/>绑定规则：第一个为列表，第二个为需要判断的元素对象<para/>
	/// 参数规则：【true返回值:false返回值:值类型(<see cref="ConvertResultType"/>的字符串形式)】
	/// 其中值类型取值：1-Visibility，2-Bool，3-String，4-Int，5-Float，6-Double <para/>
	///	返回规则：绑定内容数小于2，或者绑定的第一个对象不是 <see cref="IList"/> 类型，或者列表对象为空，则返回 false返回值。
	/// 否则依据属性 <see cref="HeadOrTail"/> 结合实际情况返回对象是否在列表的头或尾，是的话返回 true返回值。
	/// </remarks>
	/// <example>
	///	<![CDATA[
	/// <Separator.Visibility>
	///		<MultiBinding Converter="{StaticResource IsTailOfListMultiConverter}" ConverterParameter="Hidden:Visible:1">
	/// 		<Binding Path="ItemsSource" RelativeSource="{RelativeSource AncestorType=ListView}"/>
	/// 		<Binding/>
	/// 	</MultiBinding>
	/// </Separator.Visibility>
	/// ]]>
	/// </example>
	public class IsHeadOrTailOfListMultiConverter : IMultiValueConverter
	{
		/// <summary>
		/// 需要判断的是头还是尾（头-true，[默认]尾-false）
		/// </summary>
		public bool HeadOrTail { get; set; }

		/// <inheritdoc />
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				string paraStr = parameter + "";
				//将参数字符串分段，[0] 为 true返回值，[1] 为 false返回值，[2] 为转换类型枚举的字符串形式;
				string[] paraArray = paraStr.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

				if(string.IsNullOrWhiteSpace(paraStr) || paraArray.Length < 2)
				{
					Console.WriteLine($"参数[{paraStr}]非法，需包含':'符且具有合适的内容。已替换为\"true:false\"。");
					paraStr = "true:false:3";
					paraArray = paraStr.Split(':');
				}
				else if(paraArray.Length == 2)
				{
					paraStr += ":3";
					paraArray = paraStr.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				}

				bool isHeadOrTailOfList = false;
				if(values.Length >= 2 && values[0] is IList list)
				{
					int listLen = list.Count;
					var element = values[1];
					if(listLen > 0)
					{
						if(HeadOrTail)
						{
							if(list[0] == element)
								isHeadOrTailOfList = true;
						}
						else
						{
							if(list[listLen - 1] == element)
								isHeadOrTailOfList = true;
						}
					}
				}

				string resultStr = isHeadOrTailOfList ? paraArray[0] : paraArray[1];
				return ConverterHelper.ConvertValue(resultStr, paraArray[2]);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
				return ex.Message;
			}
		}

		/// <inheritdoc />
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
