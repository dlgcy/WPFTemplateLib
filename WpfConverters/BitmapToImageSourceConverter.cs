﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using WPFTemplateLib.WpfHelpers;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// [转换器] Bitmap 转为 ImageSource（BitmapSource）。
    /// （使用 BitmapHelper 中的方法，参数传[A,B,C]中的一项，不传默认C方法）
    /// </summary>
    public class BitmapToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value == null)
                {
                    return null;
                }

                Type sourceType = value.GetType();
                if (sourceType != typeof(Bitmap))
                {
                    throw new ArgumentException($"不支持的数据类型【{sourceType}】", nameof(value));
                }

                Bitmap bitmap = (Bitmap)value;
                ImageSource imageSource = null;
                switch (parameter + "")
                {
                    case "A":
                        imageSource = BitmapHelper.ToBitmapSourceA(bitmap);
                        break;
                    case "B":
                        imageSource = BitmapHelper.ToBitmapSourceB(bitmap);
                        break;
                    default:
                    case "C":
                        imageSource = BitmapHelper.ToBitmapSourceC(bitmap);
                        break;
                }
                return imageSource;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
