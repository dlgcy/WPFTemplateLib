using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFTemplateLib.Controls
{
	/// <summary>
	/// [自定义控件] 圆环带内外文字
	/// </summary>
	public class CircleWithInOutText : Control
	{
		static CircleWithInOutText()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(CircleWithInOutText), new FrameworkPropertyMetadata(typeof(CircleWithInOutText)));
		}

		#region 依赖属性

		#region [依赖属性] 圆环放置位置
		/// <summary>
		/// [依赖属性] 圆环放置位置
		/// </summary>
		public Dock CirclePlacement
		{
			get => (Dock)GetValue(CirclePlacementProperty);
			set => SetValue(CirclePlacementProperty, value);
		}
		public static readonly DependencyProperty CirclePlacementProperty =
			DependencyProperty.Register(nameof(CirclePlacement), typeof(Dock), typeof(CircleWithInOutText), new PropertyMetadata(Dock.Left));
		#endregion

		#region [依赖属性] 圆环直径
		/// <summary>
		/// [依赖属性] 圆环直径
		/// </summary>
		public double CircleDiameter
		{
			get => (double)GetValue(CircleDiameterProperty);
			set => SetValue(CircleDiameterProperty, value);
		}
		public static readonly DependencyProperty CircleDiameterProperty =
			DependencyProperty.Register(nameof(CircleDiameter), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(32d));
		#endregion

		#region [依赖属性] 圆环颜色
		/// <summary>
		/// [依赖属性] 圆环颜色
		/// </summary>
		public Brush CircleStroke
		{
			get => (Brush)GetValue(CircleStrokeProperty);
			set => SetValue(CircleStrokeProperty, value);
		}
		public static readonly DependencyProperty CircleStrokeProperty =
			DependencyProperty.Register(nameof(CircleStroke), typeof(Brush), typeof(CircleWithInOutText), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 圆环粗细
		/// <summary>
		/// [依赖属性] 圆环粗细
		/// </summary>
		public double CircleStrokeThickness
		{
			get => (double)GetValue(CircleStrokeThicknessProperty);
			set => SetValue(CircleStrokeThicknessProperty, value);
		}
		public static readonly DependencyProperty CircleStrokeThicknessProperty =
			DependencyProperty.Register(nameof(CircleStrokeThickness), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(1d));
		#endregion

		#region [依赖属性] 圆环内文字
		/// <summary>
		/// [依赖属性] 圆环内文字
		/// </summary>
		public string InnerText
		{
			get => (string)GetValue(InnerTextProperty);
			set => SetValue(InnerTextProperty, value);
		}
		public static readonly DependencyProperty InnerTextProperty =
			DependencyProperty.Register(nameof(InnerText), typeof(string), typeof(CircleWithInOutText), new PropertyMetadata(""));
		#endregion

		#region [依赖属性] 圆环外文字
		/// <summary>
		/// [依赖属性] 圆环外文字
		/// </summary>
		public string OuterText
		{
			get => (string)GetValue(OuterTextProperty);
			set => SetValue(OuterTextProperty, value);
		}
		public static readonly DependencyProperty OuterTextProperty =
			DependencyProperty.Register(nameof(OuterText), typeof(string), typeof(CircleWithInOutText), new PropertyMetadata(""));
		#endregion

		#region [依赖属性] 圆环内文字颜色
		/// <summary>
		/// [依赖属性] 圆环内文字颜色
		/// </summary>
		public Brush InnerTextColor
		{
			get => (Brush)GetValue(InnerTextColorProperty);
			set => SetValue(InnerTextColorProperty, value);
		}
		public static readonly DependencyProperty InnerTextColorProperty =
			DependencyProperty.Register(nameof(InnerTextColor), typeof(Brush), typeof(CircleWithInOutText), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 圆环外文字颜色
		/// <summary>
		/// [依赖属性] 圆环外文字颜色
		/// </summary>
		public Brush OuterTextColor
		{
			get => (Brush)GetValue(OuterTextColorProperty);
			set => SetValue(OuterTextColorProperty, value);
		}
		public static readonly DependencyProperty OuterTextColorProperty =
			DependencyProperty.Register(nameof(OuterTextColor), typeof(Brush), typeof(CircleWithInOutText), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 文字颜色（统一设置圈内外文字颜色）
		/// <summary>
		/// 文字颜色（统一设置圈内外文字颜色）
		/// </summary>
		public Brush TextColor
		{
			get => (Brush)GetValue(TextColorProperty);
			set => SetValue(TextColorProperty, value);
		}
		/// <summary>
		/// [依赖属性] 文字颜色（统一设置圈内外文字颜色）
		/// </summary>
		public static readonly DependencyProperty TextColorProperty =
			 DependencyProperty.Register(nameof(TextColor), typeof(Brush), typeof(CircleWithInOutText), new PropertyMetadata(new SolidColorBrush(Colors.Black), TextColorChangedCallback));
		/// <summary>
		/// 文字颜色（统一设置圈内外文字颜色） 变动处理方法
		/// </summary>
		private static void TextColorChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = (CircleWithInOutText)d;
			Brush brush = (Brush)e.NewValue;
			control.InnerTextColor = brush;
			control.OuterTextColor = brush;
		}
		#endregion

		#region [依赖属性] 圆环内颜色（默认透明）
		/// <summary>
		/// [依赖属性] 圆环内颜色（默认透明）
		/// </summary>
		public Brush CircleInnerColor
		{
			get => (Brush)GetValue(CircleInnerColorProperty);
			set => SetValue(CircleInnerColorProperty, value);
		}
		public static readonly DependencyProperty CircleInnerColorProperty =
			DependencyProperty.Register(nameof(CircleInnerColor), typeof(Brush), typeof(CircleWithInOutText), new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));
		#endregion

		#region [依赖属性] 圆环和外文字的距离（默认5）
		/// <summary>
		/// [依赖属性] 圆环和外文字的距离（默认5）
		/// </summary>
		public double DistanceBetweenCircleAndText
		{
			get => (double)GetValue(DistanceBetweenCircleAndTextProperty);
			set => SetValue(DistanceBetweenCircleAndTextProperty, value);
		}
		public static readonly DependencyProperty DistanceBetweenCircleAndTextProperty =
			DependencyProperty.Register(nameof(DistanceBetweenCircleAndText), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(5d));
		#endregion

		#region [依赖属性] 圆环内文字字号
		/// <summary>
		/// [依赖属性] 圆环内文字字号
		/// </summary>
		public double InnerTextFontSize
		{
			get => (double)GetValue(InnerTextFontSizeProperty);
			set => SetValue(InnerTextFontSizeProperty, value);
		}
		public static readonly DependencyProperty InnerTextFontSizeProperty =
			DependencyProperty.Register(nameof(InnerTextFontSize), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(SystemFonts.MessageFontSize));
		#endregion

		#region [依赖属性] 圆环外文字字号
		/// <summary>
		/// [依赖属性] 圆环外文字字号
		/// </summary>
		public double OuterTextFontSize
		{
			get => (double)GetValue(OuterTextFontSizeProperty);
			set => SetValue(OuterTextFontSizeProperty, value);
		}
		public static readonly DependencyProperty OuterTextFontSizeProperty =
			DependencyProperty.Register(nameof(OuterTextFontSize), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(SystemFonts.MessageFontSize));
		#endregion

		#region [依赖属性] 文字字号（统一设置圆环内外）
		/// <summary>
		/// 文字字号（统一设置圆环内外）
		/// </summary>
		public double TextFontSize
		{
			get => (double)GetValue(TextFontSizeProperty);
			set => SetValue(TextFontSizeProperty, value);
		}
		/// <summary>
		/// [依赖属性] 文字字号（统一设置圆环内外）
		/// </summary>
		public static readonly DependencyProperty TextFontSizeProperty =
			 DependencyProperty.Register(nameof(TextFontSize), typeof(double), typeof(CircleWithInOutText), new PropertyMetadata(SystemFonts.MessageFontSize, TextFontSizeChangedCallback));
		/// <summary>
		/// 文字字号（统一设置圆环内外） 变动处理方法
		/// </summary>
		private static void TextFontSizeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = (CircleWithInOutText)d;
			double fontSize = (double)e.NewValue;
			control.InnerTextFontSize = fontSize;
			control.OuterTextFontSize = fontSize;
		}
		#endregion

		#region [依赖属性] 圆环内文字字重
		/// <summary>
		/// [依赖属性] 圆环内文字字重
		/// </summary>
		public FontWeight InnerTextFontWeight
		{
			get => (FontWeight)GetValue(InnerTextFontWeightProperty);
			set => SetValue(InnerTextFontWeightProperty, value);
		}
		public static readonly DependencyProperty InnerTextFontWeightProperty =
			DependencyProperty.Register(nameof(InnerTextFontWeight), typeof(FontWeight), typeof(CircleWithInOutText), new PropertyMetadata(FontWeights.Normal));
		#endregion

		#region [依赖属性] 圆环外文字字重
		/// <summary>
		/// [依赖属性] 圆环外文字字重
		/// </summary>
		public FontWeight OuterTextFontWeight
		{
			get => (FontWeight)GetValue(OuterTextFontWeightProperty);
			set => SetValue(OuterTextFontWeightProperty, value);
		}
		public static readonly DependencyProperty OuterTextFontWeightProperty =
			DependencyProperty.Register(nameof(OuterTextFontWeight), typeof(FontWeight), typeof(CircleWithInOutText), new PropertyMetadata(FontWeights.Normal));
		#endregion

		#region [依赖属性] 文字字重（统一设置圆环内外文字字重）
		/// <summary>
		/// 文字字重（统一设置圆环内外文字字重）
		/// </summary>
		public FontWeight TextFontWeight
		{
			get => (FontWeight)GetValue(TextFontWeightProperty);
			set => SetValue(TextFontWeightProperty, value);
		}
		/// <summary>
		/// [依赖属性] 文字字重（统一设置圆环内外文字字重）
		/// </summary>
		public static readonly DependencyProperty TextFontWeightProperty =
			 DependencyProperty.Register(nameof(TextFontWeight), typeof(FontWeight), typeof(CircleWithInOutText), new PropertyMetadata(FontWeights.Normal, TextFontWeightChangedCallback));
		/// <summary>
		/// 文字字重（统一设置圆环内外文字字重） 变动处理方法
		/// </summary>
		private static void TextFontWeightChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var control = (CircleWithInOutText)d;
			FontWeight fontWeight = (FontWeight)e.NewValue;
			control.InnerTextFontWeight = fontWeight;
			control.OuterTextFontWeight = fontWeight;
		}
		#endregion

		#endregion
	}
}
