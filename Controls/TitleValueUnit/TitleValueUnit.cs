using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WPFTemplateLib.Controls
{
	/// <summary>
	/// [自定义控件] 标题-值-单位 区块
	/// </summary>
	public class TitleValueUnit : Control
	{
		static TitleValueUnit()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(TitleValueUnit), new FrameworkPropertyMetadata(typeof(TitleValueUnit)));
		}

		#region [依赖属性] 标题
		/// <summary>
		/// [依赖属性] 标题
		/// </summary>
		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}
		public static readonly DependencyProperty TitleProperty =
			DependencyProperty.Register(nameof(Title), typeof(string), typeof(TitleValueUnit), new PropertyMetadata(string.Empty));
		#endregion

		#region [依赖属性] 值
		/// <summary>
		/// [依赖属性] 值
		/// </summary>
		public object Value
		{
			get => (object)GetValue(ValueProperty);
			set => SetValue(ValueProperty, value);
		}
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register(nameof(Value), typeof(object), typeof(TitleValueUnit), new PropertyMetadata(null));
		#endregion

		#region [依赖属性] 单位
		/// <summary>
		/// [依赖属性] 单位
		/// </summary>
		public string Unit
		{
			get => (string)GetValue(UnitProperty);
			set => SetValue(UnitProperty, value);
		}
		public static readonly DependencyProperty UnitProperty =
			DependencyProperty.Register(nameof(Unit), typeof(string), typeof(TitleValueUnit), new PropertyMetadata(string.Empty));
		#endregion

		#region [依赖属性] 供外部绑定的命令
		/// <summary>
		/// [依赖属性]供外部绑定的命令
		/// </summary>
		public ICommand SetValueCommand
		{
			get => (ICommand)GetValue(SetValueCommandProperty);
			set => SetValue(SetValueCommandProperty, value);
		}
		public static readonly DependencyProperty SetValueCommandProperty =
			DependencyProperty.Register(nameof(SetValueCommand), typeof(ICommand), typeof(TitleValueUnit));
		#endregion

		#region [依赖属性] 标题位置（默认顶部）
		/// <summary>
		/// [依赖属性] 标题位置（默认顶部）
		/// </summary>
		public Dock TitlePosition
		{
			get => (Dock)GetValue(TitlePositionProperty);
			set => SetValue(TitlePositionProperty, value);
		}
		public static readonly DependencyProperty TitlePositionProperty =
			DependencyProperty.Register(nameof(TitlePosition), typeof(Dock), typeof(TitleValueUnit), new PropertyMetadata(Dock.Top));
		#endregion

		#region [依赖属性] 标题边距（默认 0,0,0,8）
		/// <summary>
		/// [依赖属性] 标题边距（默认 0,0,0,8）
		/// </summary>
		public Thickness TitleMargin
		{
			get => (Thickness)GetValue(TitleMarginProperty);
			set => SetValue(TitleMarginProperty, value);
		}
		public static readonly DependencyProperty TitleMarginProperty =
			DependencyProperty.Register(nameof(TitleMargin), typeof(Thickness), typeof(TitleValueUnit), new PropertyMetadata(new Thickness(0,0,0,8)));
		#endregion

		#region [依赖属性] 标题宽度
		/// <summary>
		/// [依赖属性] 标题宽度
		/// </summary>
		public double TitleWidth
		{
			get => (double)GetValue(TitleWidthProperty);
			set => SetValue(TitleWidthProperty, value);
		}
		public static readonly DependencyProperty TitleWidthProperty =
			DependencyProperty.Register(nameof(TitleWidth), typeof(double), typeof(TitleValueUnit), new PropertyMetadata(double.NaN));
		#endregion

		#region [依赖属性] 标题对齐方式（默认左对齐）
		/// <summary>
		/// [依赖属性] 标题对齐方式（默认左对齐）
		/// </summary>
		public TextAlignment TitleTextAlignment
		{
			get => (TextAlignment)GetValue(TitleTextAlignmentProperty);
			set => SetValue(TitleTextAlignmentProperty, value);
		}
		public static readonly DependencyProperty TitleTextAlignmentProperty =
			DependencyProperty.Register(nameof(TitleTextAlignment), typeof(TextAlignment), typeof(TitleValueUnit), new PropertyMetadata(TextAlignment.Left));
		#endregion

		#region [依赖属性] 值显示区域边距（默认 12,0,0,0）
		/// <summary>
		/// [依赖属性] 值显示区域边距（默认 12,0,0,0）
		/// </summary>
		public Thickness ValueRegionMargin
		{
			get => (Thickness)GetValue(ValueRegionMarginProperty);
			set => SetValue(ValueRegionMarginProperty, value);
		}
		public static readonly DependencyProperty ValueRegionMarginProperty =
			DependencyProperty.Register(nameof(ValueRegionMargin), typeof(Thickness), typeof(TitleValueUnit), new PropertyMetadata(new Thickness(12, 0, 0, 0)));
		#endregion

		#region [依赖属性] 内容字符格式化
		/// <summary>
		/// [依赖属性] 内容字符格式化
		/// </summary>
		public string ContentStringFormat
		{
			get => (string)GetValue(ContentStringFormatProperty);
			set => SetValue(ContentStringFormatProperty, value);
		}
		public static readonly DependencyProperty ContentStringFormatProperty =
			DependencyProperty.Register(nameof(ContentStringFormat), typeof(string), typeof(TitleValueUnit), new PropertyMetadata(string.Empty));
		#endregion
	}
}
