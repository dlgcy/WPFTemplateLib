using System;
using System.Windows;
using System.Windows.Input;

/*
 * https://gitee.com/DLGCY_Clone/WpfToast
 * https://gitee.com/dlgcy/WPFTemplateLib
 */
namespace WPFTemplateLib.Controls.WpfToast
{
	public partial class ToastTextBlock : TextBlockEx
	{
        public ToastTextBlock()
        {
            InitializeComponent();
        }

        #region 点击弹出Toast

        private void ToastTextBlock_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!IsTextTrimmed)
            {
                return;
            }

            string msg = Text;
            if (string.IsNullOrWhiteSpace(msg))
            {
                return;
            }

            Toast.Show(msg, new ToastOptions()
            {
                Icon = ToastIcons.Information,
                Location = ToastLocation.ScreenCenter,
                Time = 3000,
                FontSize = 20,
                FontWeight = FontWeights.Bold,
                Click = ToastClick,
                TextWidth = 1000,
            });
        }

        private void ToastClick(object sender, EventArgs e)
        {
            Toast toast = sender as Toast;
            toast?.Close();
        }

        #endregion
    }
}
