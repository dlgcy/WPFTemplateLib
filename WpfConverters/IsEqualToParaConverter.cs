using System;
using System.Globalization;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 使用 Equals 方法比较值和参数
	/// </summary>
	public class IsEqualToParaConverter : IValueConverter
	{
		#region Implementation of IValueConverter

		/// <inheritdoc />
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Equals(value, parameter);
		}

		/// <inheritdoc />
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
