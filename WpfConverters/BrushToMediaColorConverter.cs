using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] SolidColorBrush 转 System.Windows.Media.Color。（参数可传默认颜色）
	/// </summary>
	public class BrushToMediaColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Color color = Colors.Black;
			if(parameter != null)
			{
				try
				{
					SolidColorBrush defaultBrush = (SolidColorBrush)parameter;
					color = defaultBrush.Color;
				}
				catch(Exception ex)
				{
					Console.WriteLine(ex);
					Debug.WriteLine(ex.ToString());
				}
			}

			try
			{
				color = ((SolidColorBrush)value).Color;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
				Debug.WriteLine(ex.ToString());
			}

			return color;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
