using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WPFTemplateLib.UserControls
{
	/// <summary>
	/// UcCircleWithTextBlock.xaml 的交互逻辑
	/// </summary>
	public partial class UcCircleWithTextBlock : UserControl
	{
		public UcCircleWithTextBlock()
		{
			InitializeComponent();
		}

		#region [依赖属性] 圆环放置位置
		/// <summary>
		/// [依赖属性] 圆环放置位置
		/// </summary>
		public Dock CirclePlacement
		{
			get => (Dock)GetValue(CirclePlacementProperty);
			set => SetValue(CirclePlacementProperty, value);
		}
		public static readonly DependencyProperty CirclePlacementProperty =
			DependencyProperty.Register(nameof(CirclePlacement), typeof(Dock), typeof(UcCircleWithTextBlock), new PropertyMetadata(Dock.Left));
		#endregion

		#region [依赖属性] 圆环直径
		/// <summary>
		/// [依赖属性] 圆环直径
		/// </summary>
		public double CircleDiameter
		{
			get => (double)GetValue(CircleDiameterProperty);
			set => SetValue(CircleDiameterProperty, value);
		}
		public static readonly DependencyProperty CircleDiameterProperty =
			DependencyProperty.Register(nameof(CircleDiameter), typeof(double), typeof(UcCircleWithTextBlock), new PropertyMetadata(32d));
		#endregion

		#region [依赖属性] 圆环颜色
		/// <summary>
		/// [依赖属性] 圆环颜色
		/// </summary>
		public Brush CircleStroke
		{
			get => (Brush)GetValue(CircleStrokeProperty);
			set => SetValue(CircleStrokeProperty, value);
		}
		public static readonly DependencyProperty CircleStrokeProperty =
			DependencyProperty.Register(nameof(CircleStroke), typeof(Brush), typeof(UcCircleWithTextBlock), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 圆环粗细
		/// <summary>
		/// [依赖属性] 圆环粗细
		/// </summary>
		public double CircleStrokeThickness
		{
			get => (double)GetValue(CircleStrokeThicknessProperty);
			set => SetValue(CircleStrokeThicknessProperty, value);
		}
		public static readonly DependencyProperty CircleStrokeThicknessProperty =
			DependencyProperty.Register(nameof(CircleStrokeThickness), typeof(double), typeof(UcCircleWithTextBlock), new PropertyMetadata(1d));
		#endregion

		#region [依赖属性] 圆环内文字
		/// <summary>
		/// [依赖属性] 圆环内文字
		/// </summary>
		public string InnerText
		{
			get => (string)GetValue(InnerTextProperty);
			set => SetValue(InnerTextProperty, value);
		}
		public static readonly DependencyProperty InnerTextProperty =
			DependencyProperty.Register(nameof(InnerText), typeof(string), typeof(UcCircleWithTextBlock), new PropertyMetadata(""));
		#endregion

		#region [依赖属性] 圆环外文字
		/// <summary>
		/// [依赖属性] 圆环外文字
		/// </summary>
		public string OuterText
		{
			get => (string)GetValue(OuterTextProperty);
			set => SetValue(OuterTextProperty, value);
		}
		public static readonly DependencyProperty OuterTextProperty =
			DependencyProperty.Register(nameof(OuterText), typeof(string), typeof(UcCircleWithTextBlock), new PropertyMetadata(""));
		#endregion

		#region [依赖属性] 圆环内文字颜色
		/// <summary>
		/// [依赖属性] 圆环内文字颜色
		/// </summary>
		public Brush InnerTextColor
		{
			get => (Brush)GetValue(InnerTextColorProperty);
			set => SetValue(InnerTextColorProperty, value);
		}
		public static readonly DependencyProperty InnerTextColorProperty =
			DependencyProperty.Register(nameof(InnerTextColor), typeof(Brush), typeof(UcCircleWithTextBlock), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 圆环外文字颜色
		/// <summary>
		/// [依赖属性] 圆环外文字颜色
		/// </summary>
		public Brush OuterTextColor
		{
			get => (Brush)GetValue(OuterTextColorProperty);
			set => SetValue(OuterTextColorProperty, value);
		}
		public static readonly DependencyProperty OuterTextColorProperty =
			DependencyProperty.Register(nameof(OuterTextColor), typeof(Brush), typeof(UcCircleWithTextBlock), new PropertyMetadata(new SolidColorBrush(Colors.Black)));
		#endregion

		#region [依赖属性] 圆环内颜色（默认透明）
		/// <summary>
		/// [依赖属性] 圆环内颜色（默认透明）
		/// </summary>
		public Brush CircleInnerColor
		{
			get => (Brush)GetValue(CircleInnerColorProperty);
			set => SetValue(CircleInnerColorProperty, value);
		}
		public static readonly DependencyProperty CircleInnerColorProperty =
			DependencyProperty.Register(nameof(CircleInnerColor), typeof(Brush), typeof(UcCircleWithTextBlock), new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));
		#endregion
	}
}
