using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
/*
 * 源码已托管：https://gitee.com/dlgcy/WPFTemplate
 * 版本：2024年11月21日
 */
namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// 绑定值与参数 ToString 比较后转为是否显示（参数可用“|”分隔表示“或”的关系）
	/// </summary>
	/// <example>
	/// Visibility="{Binding EditType, Converter={StaticResource ValueToVisibleConverter}, ConverterParameter=Edit|Add}"
	/// </example>
	public class ValueToVisibleConverter : IValueConverter
	{
		/// <summary>
		/// 是否在绑定的 Value 值为空时显示（默认 false）
		/// </summary>
		public bool IsValueEmptyAsVisible { get; set; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			//if(string.IsNullOrWhiteSpace(parameter + ""))
			//	throw new ArgumentNullException(nameof(parameter), "parameter can not be null or empty");

			if(string.IsNullOrWhiteSpace(value + ""))
			{
				Console.WriteLine($"[{nameof(ValueToVisibleConverter)}] Warning：value is null or empty");
				if(IsValueEmptyAsVisible)
				{
					return Visibility.Visible;
				}
			}

			List<string> paraList = (parameter + "").Split('|').ToList();
			if(paraList.Exists(x => value + "" == x))
				return Visibility.Visible;
			else
				return Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// 绑定值与参数 ToString 比较后转为是否隐藏（参数可用“|”分隔表示“或”的关系）
	/// </summary>
	public class ValueToCollapsedConverter : IValueConverter
	{
		/// <summary>
		/// 是否在绑定的 Value 值为空时隐藏（默认 false）
		/// </summary>
		public bool IsValueEmptyAsCollapsed { get; set; }

		/// <summary>
		/// 隐藏是 Collapsed 还是 Hidden（默认 true-Collapsed）
		/// </summary>
		public bool IsCollapsedOrHidden { get; set; } = true;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value == null)
				return Visibility.Collapsed;

			//if(string.IsNullOrWhiteSpace(parameter + ""))
			//	throw new ArgumentNullException(nameof(parameter), "parameter can not be null or empty");

			if(string.IsNullOrWhiteSpace(value + ""))
			{
				Console.WriteLine($"[{nameof(ValueToCollapsedConverter)}] Warning：value is null or empty");
				if(IsValueEmptyAsCollapsed)
				{
					return IsCollapsedOrHidden ? Visibility.Collapsed : Visibility.Hidden;
				}
			}

			List<string> paraList = (parameter + "").Split('|').ToList();
			if(paraList.Exists(x => value + "" == x))
				return IsCollapsedOrHidden ? Visibility.Collapsed : Visibility.Hidden;
			else
				return Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
