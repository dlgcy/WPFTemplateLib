using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 显示和隐藏相互转换
	/// <example>
	///	<code>
	///	//界面按钮直接控制元素显式隐藏
	/// &lt;b:Interaction.Triggers>
	/// 	&lt;b:EventTrigger EventName="Click">
	/// 		&lt;b:ChangePropertyAction PropertyName="Visibility" TargetName="thePanel" 
	/// 			Value="{Binding Visibility, ElementName=thePanel, Converter={StaticResource VisibleCollapsedSwitchConverter}}">
	/// 		&lt;/b:ChangePropertyAction>
	/// 	&lt;/b:EventTrigger>
	/// &lt;/b:Interaction.Triggers>
	/// </code>
	/// </example>
	/// </summary>
	public class VisibleCollapsedSwitchConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			switch (value)
			{
				case Visibility.Visible:
					return Visibility.Collapsed;
				default:
				case Visibility.Collapsed:
					return Visibility.Visible;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
