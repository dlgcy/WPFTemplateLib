﻿using System.Windows;
using System.Windows.Controls;

namespace WPFTemplateLib.Attached
{
	/// <summary>
	/// DataGrid 附加属性帮助类
	/// </summary>
	public class DataGridAttached
    {
        #region 是否显示行号（IsShowRowNumber）

        //https://www.cnblogs.com/luqingfei/p/12697212.html

        public static bool GetIsShowRowNumber(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsShowRowNumberProperty);
		}

        /// <summary>
        /// 是否显示行号（在 Row.Header 中）
        /// </summary>
        /// <example>
        ///	<code>
        ///	{Binding Header, RelativeSource={RelativeSource AncestorType=DataGridRow}}
        /// </code>
        /// </example>
        [AttachedPropertyBrowsableForType(typeof(DataGrid))]
		public static void SetIsShowRowNumber(DependencyObject obj, bool value)
		{
			obj.SetValue(IsShowRowNumberProperty, value);
		}

		/// <summary>
		/// 设置是否显示行号
		/// </summary>
		public static readonly DependencyProperty IsShowRowNumberProperty =
			DependencyProperty.RegisterAttached("IsShowRowNumber", typeof(bool), typeof(DataGridAttached),
				new PropertyMetadata(false, OnIsShowRowNumberChanged));

		private static void OnIsShowRowNumberChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			DataGrid grid = d as DataGrid;
			if (grid == null)
			{
				return;
			}

			if ((bool)e.NewValue)
			{
				grid.LoadingRow += OnGridLoadingRow;
				grid.UnloadingRow += OnGridUnloadingRow;
			}
			else
			{
				grid.LoadingRow -= OnGridLoadingRow;
				grid.UnloadingRow -= OnGridUnloadingRow;
			}
		}

		private static void RefreshDataGridRowNumber(object sender)
		{
			DataGrid grid = sender as DataGrid;
			if (grid == null)
			{
				return;
			}

			foreach (var item in grid.Items)
			{
				var row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromItem(item);
				if (row == null)
				{
					return;
				}
				row.Header = row.GetIndex() + 1;
			}
		}

		private static void OnGridUnloadingRow(object sender, DataGridRowEventArgs e)
		{
			RefreshDataGridRowNumber(sender);
		}

		private static void OnGridLoadingRow(object sender, DataGridRowEventArgs e)
		{
			RefreshDataGridRowNumber(sender);
		}

		#endregion
    }
}
