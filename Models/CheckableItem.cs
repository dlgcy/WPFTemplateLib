using System;
using System.ComponentModel;
using WPFTemplateLib.Mvvm;

namespace WPFTemplateLib.Models
{
	/// <summary>
	/// 可勾选项
	/// </summary>
	public class CheckableItem : SimpleBindableBase
	{
		#region 成员、构造

		/// <summary>
		/// 勾选状态改变事件
		/// </summary>
		public event Action<CheckableItem> CheckStatusChanged;

		/// <summary>
		/// 构造
		/// </summary>
		/// <param name="tag">标识（会同时设置 ShowName）</param>
		public CheckableItem(string tag)
		{
			Tag = tag;
			ShowName = Tag;
		}

		/// <summary>
		/// 构造
		/// </summary>
		/// <param name="tag">标识</param>
		/// <param name="showName">显示名称</param>
		public CheckableItem(string tag, string showName)
		{
			Tag = tag;
			ShowName = showName;
		}

		/// <summary>
		/// 构造
		/// </summary>
		/// <param name="tag">标识</param>
		/// <param name="showName">显示名称</param>
		/// <param name="groupName">组名</param>
		public CheckableItem(string tag, string showName, string groupName)
		{
			Tag = tag;
			ShowName = showName;
			GroupName = groupName;
		}

		#endregion

		#region 属性

		/// <summary>
		/// 是否勾选
		/// </summary>
		public bool IsChecked { get; set; }

		/// <summary>
		/// 是否选中
		/// </summary>
		public bool IsSelected { get; set; }

		/// <summary>
		/// 标识
		/// </summary>
		public string Tag { get; set; }

		/// <summary>
		/// 显示名称
		/// </summary>
		public string ShowName { get; set; }

		/// <summary>
		/// 组名
		/// </summary>
		public string GroupName { get; set; } = string.Empty;

		/// <summary>
		/// 数据
		/// </summary>
		public object Data { get; set; }

		#endregion

		#region 方法

		/// <inheritdoc />
		protected override void PropertyChangedHandle(object sender, PropertyChangedEventArgs e)
		{
			base.PropertyChangedHandle(sender, e);
			switch (e.PropertyName)
			{
				case nameof(IsChecked):
				{
					CheckStatusChanged?.Invoke(this);
					break;
				}
			}
		}

		#endregion
	}
}
