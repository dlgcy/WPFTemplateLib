using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 判断字符串是否为空
	/// </summary>
	public class IsNullOrWhitespaceConverter : DependencyObject, IValueConverter
	{
		/// <inheritdoc />
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (IsInverse)
			{
				return !string.IsNullOrWhiteSpace(value + "");
			}

			return string.IsNullOrWhiteSpace(value + "");
		}

		public static readonly DependencyProperty IsInverseProperty = DependencyProperty.Register(
			nameof(IsInverse), typeof(bool), typeof(IsNullOrWhitespaceConverter), new PropertyMetadata(false));
		/// <summary>
		/// 是否翻转结果
		/// </summary>
		public bool IsInverse
		{
			get => (bool)GetValue(IsInverseProperty);
			set => SetValue(IsInverseProperty, value);
		}

		/// <inheritdoc />
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
