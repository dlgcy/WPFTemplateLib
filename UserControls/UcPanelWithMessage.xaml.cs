﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WPFTemplateLib.UserControls
{
	/// <summary>
	/// [用户控件] 带消息窗口的面板容器（VM 中需要有名为“Info”的字符串类型的属性，通过该属性输入新消息信息）
	/// </summary>
	[Obsolete("会有内容元素中不能命名的问题，可换用自定义控件 PanelWithMessage。")]
	public partial class UcPanelWithMessage : UserControl
	{
		public UcPanelWithMessage()
		{
			InitializeComponent();
		}

		#region 依赖属性

		/// <summary>
		/// [依赖属性] 内容区域大小（默认为 3*）
		/// </summary>
		public GridLength ContentRegionLength
		{
			get => (GridLength)GetValue(ContentRegionLengthProperty);
			set => SetValue(ContentRegionLengthProperty, value);
		}
		public static readonly DependencyProperty ContentRegionLengthProperty =
			DependencyProperty.Register(nameof(ContentRegionLength), typeof(GridLength), typeof(UcPanelWithMessage), new PropertyMetadata(new GridLength(3, GridUnitType.Star)));

		/// <summary>
		/// [依赖属性]消息区域大小
		/// </summary>
		public GridLength MessageRegionLength
		{
			get => (GridLength)GetValue(MessageRegionLengthProperty);
			set => SetValue(MessageRegionLengthProperty, value);
		}
		public static readonly DependencyProperty MessageRegionLengthProperty =
			DependencyProperty.Register(nameof(MessageRegionLength), typeof(GridLength), typeof(UcPanelWithMessage), new PropertyMetadata(new GridLength(1, GridUnitType.Star)));

		#endregion
	}
}
