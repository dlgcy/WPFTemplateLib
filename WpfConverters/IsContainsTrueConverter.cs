using System;
using System.Globalization;
using System.Windows.Data;
/*
 * 源码已托管：https://gitee.com/dlgcy/WPFTemplate
 */
namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [多值转换器][dlgcy] 是否包含真
	/// </summary>
	/// <remarks>
	///	bool 类型直接判断；string 类型判断IsNullOrWhiteSpace；其它类型使用 double.TryParse 转换，判断是否大于零。
	/// </remarks>
	public class IsContainsTrueConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values == null || values.Length == 0)
				throw new ArgumentNullException(nameof(values), "values can not be null or empty");

			bool result = false;

			foreach (object obj in values)
			{
				bool boolItem = false;

				if (obj is bool b)
				{
					boolItem = b;
				}
				else if (obj is string s)
				{
					boolItem = !string.IsNullOrWhiteSpace(s);
				}
				else if (double.TryParse(obj + "", out double d))
				{
					boolItem = d > 0;
				}

				if(boolItem)
				{
					result = true;
					break;
				}
			}

			return result;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
