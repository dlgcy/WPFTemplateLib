using System.Windows;
using System.Windows.Media;
using WPFTemplateLib.WpfHelpers;

namespace WPFTemplateLib.Attached.AttachGroup
{
	public static class WatermarkElement
    {
		#region [附加属性] 水印文本
		public static string GetWatermarkText(DependencyObject obj)
		{
			return (string)obj.GetValue(WatermarkTextProperty);
		}
		/// <summary>
		/// 设置 水印文本
		/// </summary>
		public static void SetWatermarkText(DependencyObject obj, string value)
		{
			obj.SetValue(WatermarkTextProperty, value);
		}
		/// <summary>
		/// [附加属性] 水印文本
		/// </summary>
		public static readonly DependencyProperty WatermarkTextProperty =
			DependencyProperty.RegisterAttached("WatermarkText", typeof(string), typeof(WatermarkElement), new PropertyMetadata(string.Empty));
		#endregion

		#region [附加属性] 水印颜色画刷
		public static Brush GetWatermarkBrush(DependencyObject obj)
		{
			return (Brush)obj.GetValue(WatermarkBrushProperty);
		}
		/// <summary>
		/// 设置 水印颜色画刷
		/// </summary>
		public static void SetWatermarkBrush(DependencyObject obj, Brush value)
		{
			obj.SetValue(WatermarkBrushProperty, value);
		}
		/// <summary>
		/// [附加属性] 水印颜色画刷
		/// </summary>
		public static readonly DependencyProperty WatermarkBrushProperty =
			DependencyProperty.RegisterAttached("WatermarkBrush", typeof(Brush), typeof(WatermarkElement), 
				new PropertyMetadata(new SolidColorBrush(MediaColorHelper.ColorStrToMediaColor("#B1B1B1"))));
		#endregion
	}
}
