using System;
using System.IO;
using System.Windows.Media;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows;

namespace WPFTemplateLib.WpfHelpers
{
    /// <summary>
    /// Bitmap 帮助类
    /// </summary>
    public static class BitmapHelper
    {
        #region WPF Bitmap to ImageSource 的几种方式

        /* 修改自 https://www.cnblogs.com/catzhou/p/14357646.html */

        /// <summary>
        /// Bitmap 转 ImageSource（BitmapSource）方式一（MemoryStream）
        /// </summary>
        public static ImageSource ToBitmapSourceA(Bitmap bitmap)
        {
            MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, ImageFormat.Bmp);
            stream.Position = 0;
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = stream;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);
        /// <summary>
        /// Bitmap 转 ImageSource（BitmapSource）方式二（Hbitmap）
        /// </summary>
        public static ImageSource ToBitmapSourceB(Bitmap bitmap)
        {
            IntPtr ptr = bitmap.GetHbitmap(); //obtain the Hbitmap
            BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero,
        Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr); //release the HBitmap
            return bitmapSource;
        }

        /// <summary>
        /// Bitmap 转 ImageSource（BitmapSource）方式三【推荐】（BitmapSource.Create）
        /// </summary>
        public static ImageSource ToBitmapSourceC(Bitmap bitmap)
        {
            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            BitmapPalette myPalette = GetDefaultBitmapPalette();
            BitmapSource bitmapSource = BitmapSource.Create(bitmap.Width, bitmap.Height,
                96, 96, PixelFormats.Bgr24, myPalette,
                bmpData.Scan0, bitmap.Width * bitmap.Height * 3, bitmap.Width * 3);
            bitmap.UnlockBits(bmpData);
            return bitmapSource;
        }

        ///// <summary>
        ///// OpenCvSharp.Mat 转 ImageSource（BitmapSource）
        ///// </summary>
        //public static ImageSource ToBitmapSourceD(OpenCvSharp.Mat frame)
        //{
        //    unsafe
        //    {
        //        BitmapPalette myPalette = GetDefaultBitmapPalette();
        //        return BitmapSource.Create(frame.Width, frame.Height, 96, 96, PixelFormats.Bgr24, myPalette,
        //            (IntPtr)frame.DataPointer, frame.Width * frame.Height * 3, frame.Width * 3);
        //    }
        //}

        #endregion

        /// <summary>
        /// 获取默认的 BitmapPalette
        /// </summary>
        public static BitmapPalette GetDefaultBitmapPalette()
        {
            List<System.Windows.Media.Color> colors = new List<System.Windows.Media.Color>
            {
                Colors.Red,
                Colors.Blue,
                Colors.Green
            };

            BitmapPalette myPalette = new BitmapPalette(colors);
            return myPalette;
        }
    }
}
