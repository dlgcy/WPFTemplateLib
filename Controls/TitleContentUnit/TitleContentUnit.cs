using System.Windows;
using System.Windows.Controls;

namespace WPFTemplateLib.Controls
{
	/// <summary>
	/// [自定义控件] 标题-内容-单位 区块
	/// </summary>
	public class TitleContentUnit : ContentControl
	{
		static TitleContentUnit()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(TitleContentUnit), new FrameworkPropertyMetadata(typeof(TitleContentUnit)));
		}

		#region [依赖属性] 标题
		/// <summary>
		/// [依赖属性] 标题
		/// </summary>
		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}
		public static readonly DependencyProperty TitleProperty =
			DependencyProperty.Register(nameof(Title), typeof(string), typeof(TitleContentUnit), new PropertyMetadata(string.Empty));
		#endregion

		#region [依赖属性] 单位
		/// <summary>
		/// [依赖属性] 单位
		/// </summary>
		public string Unit
		{
			get => (string)GetValue(UnitProperty);
			set => SetValue(UnitProperty, value);
		}
		public static readonly DependencyProperty UnitProperty =
			DependencyProperty.Register(nameof(Unit), typeof(string), typeof(TitleContentUnit), new PropertyMetadata(string.Empty));
		#endregion
	}
}
