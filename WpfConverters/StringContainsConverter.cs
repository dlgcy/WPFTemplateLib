using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] 判断字符串是否包含指定内容的通用转换器，参考自 ObjectConverter。
	/// </summary>
	/// <remarks>
	/// 参数规则【包含值1|包含值2|包含值n:true返回值:false返回值】<para/>
	///	value 为 null 则返回false返回值，包含值之间为或的关系
	/// </remarks>
	/// <example>
	/// <![CDATA[
	/// Visibility="{Binding Name, Converter={StaticResource StringContainsConverter}, ConverterParameter='DI|DO:Visible:Collapsed'}"
	/// ]]>
	/// </example>
	public class StringContainsConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			//将参数字符分段 parray[0]为比较值，parray[1]为true返回值，parray[2]为false返回值;
			string[] parray = (parameter + "").Split(':');

			if(value == null)
				return parray[2]; //如果数据源为空，默认返回false返回值

			string valueStr = value + "";
			if(parray[0].Contains("|")) //判断有多个比较值的情况
				return parray[0].Split('|').ToList().Exists(x => valueStr.Contains(x)) ? parray[1] : parray[2]; //多值比较

			return valueStr.Contains(parray[0]) ? parray[1] : parray[2]; //单值比较
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
