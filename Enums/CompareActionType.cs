namespace WPFTemplateLib.Enums
{
	/// <summary>
	/// 比较操作类型
	/// </summary>
	public enum CompareActionType
	{
		GreaterThan,
		GreaterThanOrEqual,
		Equal,
		LessThanOrEqual,
		LessThan
	}
}
