namespace WPFTemplateLib.Controls.Panels.HandyGrid
{
	public enum ColLayoutStatus
	{
		Xs,
		Sm,
		Md,
		Lg,
		Xl,
		Xxl,
		Auto
	}
}
