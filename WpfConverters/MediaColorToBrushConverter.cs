﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// [转换器] System.Windows.Media.Color 转 SolidColorBrush。（参数可传默认颜色）
    /// </summary>
    public class MediaColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Brush brush = new SolidColorBrush(Colors.White);
            if (parameter != null)
            {
                try
                {
                    Color defaultColor = (Color)parameter;
                    brush = new SolidColorBrush(defaultColor);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            try
            {
                brush = new SolidColorBrush((Color)value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
