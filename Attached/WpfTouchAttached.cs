﻿using System.Windows;
using WPFTemplateLib.WpfHelpers;

namespace WPFTemplateLib.Attached
{
    /// <summary>
    /// WPF触摸操作附加属性帮助类
    /// </summary>
    public class WpfTouchAttached : DependencyObject
    {
        #region 是否防止触摸事件提升为鼠标事件

        public static bool GetIsPreventTouchToMouse(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsPreventTouchToMouseProperty);
        }

        public static void SetIsPreventTouchToMouse(DependencyObject obj, bool value)
        {
            obj.SetValue(IsPreventTouchToMouseProperty, value);
        }

        /// <summary>
        /// 是否防止触摸事件提升为鼠标事件
        /// </summary>
        public static readonly DependencyProperty IsPreventTouchToMouseProperty =
            DependencyProperty.RegisterAttached("IsPreventTouchToMouse", typeof(bool), typeof(WpfTouchAttached), new PropertyMetadata(false, IsPreventTouchToMouseChanged));

        private static void IsPreventTouchToMouseChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var target = d as FrameworkElement;
            if (target == null) return;

            if ((bool)e.NewValue)
            {
                target.PreventTouchToMouse_Register();
            }
            else
            {
                target.PreventTouchToMouse_UnRegister();
            }
        }

        #endregion
    }
}
