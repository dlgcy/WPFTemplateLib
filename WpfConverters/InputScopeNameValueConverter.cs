﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// [转换器] InputScopeNameValue 转 InputScope
    /// </summary>
    public class InputScopeNameValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            InputScope inputScope = new InputScope();

            try
            {
                InputScopeNameValue inputScopeName = (InputScopeNameValue)value;
                inputScope.Names.Add(new InputScopeName(inputScopeName));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return inputScope;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
