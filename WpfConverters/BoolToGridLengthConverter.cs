﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

/*
* 源码已托管：https://gitee.com/dlgcy/WPFTemplateLib
*/
namespace WPFTemplateLib.WpfConverters
{
	/// <summary>
	/// [转换器] bool 转 Grid 行/列 中的 高/宽（<see cref="GridLength"/>）[传参版] <para/>
	/// [说明] 绑定值（或翻转后）为 false, 则 高/宽 转为 [FalseValue] 指定的值。<para/>
	/// 绑定值（或翻转后）为 true, 则 高/宽 依据 [传参] 进行转换，支持 n*、Auto、具体值。
	/// </summary>
	public class BoolToGridLengthConverter : IValueConverter
    {
		/// <summary>
		/// 绑定值（或翻转后）为 false 时对应需转换到的值
		/// </summary>
		public double FalseValue { get; set; }

        /// <summary>
        /// 绑定的 bool 值是否翻转后再计算
        /// </summary>
        public bool IsReversed { get; set; }

		/// <inheritdoc />
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GridLength gridLength = GridLength.Auto;
            try
            {
                bool calcValue = value is true;
                if (IsReversed) calcValue = !calcValue;

                if (calcValue)
                {
                    string para = parameter + "";
                    if (!string.IsNullOrWhiteSpace(para))
                    {
                        if (para == "Auto" || para == "auto")
                        {
                            //Ignore
                        }
                        else if (para.Contains("*"))
                        {
                            int index = para.IndexOf("*", StringComparison.Ordinal);
                            string numStr = para.Remove(index).Trim();
                            if (string.IsNullOrWhiteSpace(numStr))
                            {
                                numStr = "1";
                            }

                            double.TryParse(numStr, out double num);
                            if (num > 0)
                            {
                                gridLength = new GridLength(num, GridUnitType.Star);
                            }
                            else
                            {
                                gridLength = new GridLength(0);
                            }
                        }
                        else
                        {
                            double.TryParse(para.Trim(), out double num);
                            if (num > 0)
                            {
                                gridLength = new GridLength(num, GridUnitType.Pixel);
                            }
                            else
                            {
                                gridLength = new GridLength(0);
                            }
                        }
                    }
                }
                else
                {
                    gridLength = new GridLength(FalseValue);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return gridLength;
        }

		/// <inheritdoc />
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
