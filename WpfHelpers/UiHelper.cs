﻿using System.Windows;
using System.Windows.Media;

namespace WPFTemplateLib.WpfHelpers
{
	public static class UiHelper
	{
		/// <summary>
		/// 获取指定 <see cref="UIElement"/> 对象的 RenderTransform，没有时给它创建一个并赋值
		/// </summary>
		/// <typeparam name="T">Transform 子类型</typeparam>
		/// <param name="uiTarget">UI 对象</param>
		/// <returns>对象的 RenderTransform</returns>
		public static T EnsureRenderTransform<T>(UIElement uiTarget)
			where T : Transform
		{
			if (uiTarget.RenderTransform is T)
				return uiTarget.RenderTransform as T;
			else
			{
				T instance = typeof(T).Assembly.CreateInstance(typeof(T).FullName) as T;
				uiTarget.RenderTransform = instance;
				return instance;
			}
		}
	}
}
