using PropertyChanged;

namespace WPFTemplateLib.Mvvm
{
	/// <summary>
	/// 使用 PropertyChanged.Fody 包来简化属性变动通知的写法（只需要写普通的公共自动属性即可）
	/// </summary>
	/// <remarks>
	///	【注意】使用的项目中仍需安装 PropertyChanged.Fody 包
	/// </remarks>
	[AddINotifyPropertyChangedInterface]
	public class SimpleBindableBase : ObservableObject
	{
	}
}
