using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace WPFTemplateLib.Controls.WpfToast
{
	/// <summary>
	/// 气泡弹窗帮助类
	/// </summary>
	public class ToastHelper
	{
		private static Dispatcher Dispatcher => Application.Current?.Dispatcher;

		/// <summary>
		/// 气泡弹窗到屏幕中间的方法
		/// </summary>
		/// <param name="msg">消息内容</param>
		/// <param name="toastIcon">图标</param>
		/// <param name="millisecond">显示持续时间（毫秒）</param>
		/// <param name="iconForeground">图标颜色</param>
		public static void ToastToScreen(string msg, ToastIcons toastIcon = ToastIcons.Information, int millisecond = 5000, Brush iconForeground = null)
		{
			if(string.IsNullOrWhiteSpace(msg))
			{
				return;
			}

			Task.Run(delegate
			{
				Dispatcher.Invoke(delegate
				{
					var option = new ToastOptions()
					{
						Icon = toastIcon,
						Location = ToastLocation.ScreenCenter,
						Time = millisecond,
						//ToastWidth = Double.NaN﻿,
						TextWidth = 440,
					};

					if(iconForeground != null)
					{
						option.IconForeground = iconForeground;
					}

					Toast.Show(msg, option);
				});
			});
		}

		/// <summary>
		/// 气泡弹窗到屏幕中间（警告消息）
		/// </summary>
		/// <param name="msg">消息</param>
		/// <param name="millisecond">持续时间（毫秒）</param>
		public static void ToastToScreenWarning(string msg, int millisecond = 5000)
		{
			ToastToScreen(msg, ToastIcons.Warning, millisecond, new SolidColorBrush(Colors.IndianRed));
		}

		/// <summary>
		/// 气泡弹窗到屏幕中间（错误消息）
		/// </summary>
		/// <param name="msg">消息</param>
		/// <param name="millisecond">持续时间（毫秒）</param>
		public static void ToastToScreenError(string msg, int millisecond = 5000)
		{
			ToastToScreen(msg, ToastIcons.Error, millisecond, new SolidColorBrush(Colors.OrangeRed));
		}

		/// <summary>
		/// 气泡弹窗到窗口中间的方法
		/// </summary>
		/// <param name="msg">消息内容</param>
		/// <param name="win">窗口(不传则自动判断)</param>
		/// <param name="toastIcon">图标</param>
		/// <param name="millisecond">显示持续时间（毫秒）</param>
		/// <param name="iconForeground">图标颜色</param>
		public static void ToastToWindow(string msg, Window win = null, ToastIcons toastIcon = ToastIcons.Information, int millisecond = 5000, Brush iconForeground = null)
		{
			if(string.IsNullOrWhiteSpace(msg))
			{
				return;
			}

			Task.Run(delegate
			{
				Dispatcher.Invoke(delegate
				{
					var option = new ToastOptions()
					{
						Icon = toastIcon,
						Location = ToastLocation.OwnerCenter,
						Time = millisecond,
						//ToastWidth = Double.NaN﻿,
						TextWidth = 440,
					};

					if(iconForeground != null)
					{
						option.IconForeground = iconForeground;
					}

					Toast.Show(win, msg, option);
				});
			});
		}

		/// <summary>
		/// 气泡弹窗到屏幕中间（错误消息）
		/// </summary>
		/// <param name="msg">消息</param>
		/// <param name="win">窗口(不传则自动判断)</param>
		/// <param name="millisecond">持续时间（毫秒）</param>
		public static void ToastToWindowError(string msg, Window win = null, int millisecond = 5000)
		{
			ToastToWindow(msg, win, ToastIcons.Error, millisecond, new SolidColorBrush(Colors.OrangeRed));
		}
	}
}
