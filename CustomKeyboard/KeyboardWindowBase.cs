using MahApps.Metro.Controls;

namespace WPFTemplateLib.CustomKeyboard
{
	/// <summary>
	/// 键盘窗口基类
	/// </summary>
	public abstract class KeyboardWindowBase : MetroWindow
	{
		/// <summary>
		/// 获取字符串值
		/// </summary>
		public abstract string StringValue { get; }

		/// <summary>
		/// 原始值
		/// </summary>
		protected string OriginValue { get; private set; }

		protected KeyboardWindowBase (string originValue)
		{
			OriginValue = originValue;
		}
	}
}
