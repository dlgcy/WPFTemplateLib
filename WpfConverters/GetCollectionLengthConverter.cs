﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace WPFTemplateLib.WpfConverters
{
    /// <summary>
    /// 转换器：绑定集合对象，获取集合长度
    /// </summary>
    public class GetCollectionLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value is not ICollection)
            {
                return 0;
            }
            else
            {
                return ((ICollection)value).Count;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
